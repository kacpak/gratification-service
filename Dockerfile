# Service dependencies
FROM node:10-alpine as build
ENV NODE_ENV development
ENV GRATIFICATION_SERVICE_SERVE_CLIENT_SIDE true
ENV GRATIFICATION_SERVICE_CORS_ORIGIN *
ENV GRATIFICATION_SERVICE_CORS_ALLOWED_HEADERS Authorization
RUN apk --no-cache add --virtual builds-deps build-base python

WORKDIR /app/service/
COPY service/package*.json ./
RUN npm ci --prefer-offline

WORKDIR /app/client/
COPY client/package*.json ./
RUN npm ci --prefer-offline

ENV NODE_ENV production
WORKDIR /app/
COPY client client
COPY service service
COPY tslint.json ./

WORKDIR /app/service
RUN npm run build
RUN npm prune

WORKDIR /app/client
RUN npm run build

# Run image
FROM node:10-alpine
ENV NODE_ENV production
WORKDIR /app/
COPY --from=build /app/service/dist dist
COPY --from=build /app/service/node_modules node_modules
COPY --from=build /app/client/dist dist/client
COPY service/package*.json ./
EXPOSE $PORT
CMD [ "node", "dist/main.js" ]
