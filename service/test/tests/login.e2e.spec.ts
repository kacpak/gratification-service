import request from 'supertest';
import { INestApplication } from '@nestjs/common';
import { initializeApplication } from '../test.utils';

describe('Login process', () => {
  let app: INestApplication;
  let token: string;

  beforeAll(async () => {
    [app] = await initializeApplication();
  });

  afterAll(async () => {
    await app.close();
  });

  it('should login and acquire autorization token', () => {
    return request(app.getHttpServer())
      .post('/auth/login')
      .send({ username: 'demoUser', password: 'pass' })
      .expect(201)
      .expect(res => {
        token = res.body.token;
        expect(token).toBeTruthy();
      });
  });

  it('should logout', () => {
    return request(app.getHttpServer())
      .get('/auth/logout')
      .set('Authorization', `Bearer ${token}`)
      .expect(200);
  });
});
