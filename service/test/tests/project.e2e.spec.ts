import { TestingModule } from '@nestjs/testing';
import request from 'supertest';
import { INestApplication } from '@nestjs/common';
import { AchievementDto } from '../../src/modules/api/achievements/achievement.dto';
import { CallbackQueue } from '../../src/modules/api/event/callback.queue';
import { EventDto } from '../../src/modules/api/event/event.dto';
import { NewProjectDTO } from '../../src/modules/api/project/project.controller';
import { Project } from '../../src/modules/api/project/project.entity';
import { initializeApplication, logInDemoUser } from '../test.utils';
import { NewTaskDto } from '../../src/modules/api/task/task.dto';
import { NewGroupDto } from '../../src/modules/api/group/group.dto';
import { Scope } from '../../src/modules/api/achievements/achievement.entity';
import waitForExpect from 'wait-for-expect';

describe('Endpoints', () => {
  let app: INestApplication;
  let module: TestingModule;
  let token: string;
  let projectId: string;
  let achievementId: string;
  const taskId: string = '12';
  const groupIds: string[] = [];

  beforeAll(async () => {
    [app, module] = await initializeApplication();
    token = await logInDemoUser(app);
  });

  afterAll(async () => await app.close());

  const testUpdate = async <T extends object>(
    setup: (req: request.SuperTest<request.Test>) => request.Test,
    testResponse: (expected: T) => (res: any) => void,
    original: T,
    ...changes: Partial<T>[]
  ) => {
    let previous: T = original;
    for (const update of changes) {
      await setup(request(app.getHttpServer()))
        .send(update)
        .expect(200)
        .expect(
          testResponse({
            ...previous,
            ...update
          })
        )
        .expect(() => {
          previous = {
            ...previous,
            ...update
          };
        });
    }
  };

  describe('Project', () => {
    let project: Project;

    it('should create new project', () => {
      return request(app.getHttpServer())
        .post('/project')
        .set('Authorization', `Bearer ${token}`)
        .send({ name: 'Our Test Project', isABTested: true, callbackUrl: 'http://test.pl' } as NewProjectDTO)
        .expect(201)
        .expect(res => {
          expect(res.body.name).toBe('Our Test Project');
          expect(res.body.id).toBeTruthy();
          projectId = res.body.id;
          project = res.body;
        });
    });

    it('should modify project', async () => {
      await testUpdate(
        req => req.put(`/project/${projectId}`).set('Authorization', `Bearer ${token}`),
        expected => res => {
          res.body[0].name = expected.name;
          res.body[0].isABTested = expected.isABTested;
          res.body[0].callbackUrl = expected.callbackUrl;
        },
        project,
        { name: 'Other name' },
        { name: 'Test project name', callbackUrl: 'http://wp.pl' },
        { callbackUrl: 'http://test.pl/hello' },
        { callbackUrl: 'http://test.pl/callback', isABTested: true },
        { isABTested: false }
      );
    });

    it(`should list user's projects`, () => {
      return request(app.getHttpServer())
        .get('/project')
        .set('Authorization', `Bearer ${token}`)
        .expect(200)
        .expect(res => {
          expect(res.body.length).toBe(1);
          expect(res.body[0].id).toBe(projectId);
        });
    });
  });

  describe('Groups', () => {
    it(`should create new groups`, async () => {
      const newGroup1: NewGroupDto = { name: 'Group #1', description: 'new nice group' };
      const newGroup2: NewGroupDto = { name: 'Group #2', description: 'newer nice group' };

      await request(app.getHttpServer())
        .post(`/project/${projectId}/group`)
        .set('Authorization', `Bearer ${token}`)
        .send(newGroup1)
        .expect(201)
        .expect(res => {
          expect(res.body.length).toBe(1);
          expect(res.body[0].project.id).toBe(projectId);
          expect(res.body[0].name).toBe(newGroup1.name);
          expect(res.body[0].description).toBe(newGroup1.description);
          groupIds[0] = res.body[0].id;
        });

      await request(app.getHttpServer())
        .post(`/project/${projectId}/group`)
        .set('Authorization', `Bearer ${token}`)
        .send(newGroup2)
        .expect(201)
        .expect(res => {
          expect(res.body.length).toBe(1);
          expect(res.body[0].project.id).toBe(projectId);
          expect(res.body[0].name).toBe(newGroup2.name);
          expect(res.body[0].description).toBe(newGroup2.description);
          groupIds[1] = res.body[0].id;
        });
    });

    it(`should list groups`, () => {
      return request(app.getHttpServer())
        .get(`/project/${projectId}/group`)
        .set('Authorization', `Bearer ${token}`)
        .expect(200)
        .expect(res => {
          expect(res.body.length).toBe(2);
          expect(res.body[0].id).toBe(groupIds[0]);
          expect(res.body[1].id).toBe(groupIds[1]);
        });
    });

    it(`should modify groups`, async () => {
      await request(app.getHttpServer())
        .put(`/project/${projectId}/group/${groupIds[0]}`)
        .set('Authorization', `Bearer ${token}`)
        .send({
          name: 'New group #1 name'
        })
        .expect(200)
        .expect(res => {
          expect(res.body.length).toBe(1);
          expect(res.body[0].name).toBe('New group #1 name');
          groupIds[0] = res.body[0].id;
        });
    });
  });

  describe('Tasks', () => {
    it(`should create new tasks`, () => {
      const startsAt = new Date();
      const endsAt = new Date(startsAt.getFullYear(), startsAt.getMonth(), startsAt.getDate() + 7);
      const newTask: NewTaskDto = {
        name: 'Exercise #1',
        taskId,
        meta: {
          startsAt: startsAt.toISOString(),
          endsAt: endsAt.toISOString(),
          difficulty: 6
        },
        provides: ['score', 'submitDate', 'tries']
      };

      return request(app.getHttpServer())
        .post(`/project/${projectId}/group/${groupIds[1]}/task`)
        .set('Authorization', `Bearer ${token}`)
        .send(newTask)
        .expect(201)
        .expect(res => {
          expect(res.body[0].name).toBe(newTask.name);
          expect(res.body[0].localId).toBe(newTask.taskId);
          console.log(res.body);
        });
    });

    it(`should list tasks for given group`, async () => {
      await request(app.getHttpServer())
        .get(`/project/${projectId}/group/${groupIds[0]}/task`)
        .set('Authorization', `Bearer ${token}`)
        .expect(200)
        .expect(res => {
          expect(res.body.length).toBe(0);
        });

      await request(app.getHttpServer())
        .get(`/project/${projectId}/group/${groupIds[1]}/task`)
        .set('Authorization', `Bearer ${token}`)
        .expect(200)
        .expect(res => {
          expect(res.body.length).toBe(1);
        });
    });
  });

  describe('Achievements', () => {
    const getDto: () => AchievementDto = () => ({
      name: 'New Test Achievement',
      image: 'http://test.com/image.png',
      description: 'This is a new achievement for testing purposes',
      scope: Scope.TASK,
      availableInGroups: [groupIds[1]],
      triggeredBy: ['score'],
      rules: [
        {
          value: '$score',
          comparator: 'n>',
          target: 50
        }
      ]
    });

    const testAchievementResponse = (expected: AchievementDto) => (res: any) => {
      expect(res.body.length).toBe(1);
      expect(res.body[0].id).toBeTruthy();
      expect(res.body[0].description).toBe(expected.description);
      expect(res.body[0].image).toBe(expected.image);
      expect(res.body[0].projectId).toBe(projectId);
      expect(res.body[0].scope).toBe(expected.scope);
      expect(res.body[0].rulesApiVersion).toBe(expected.rulesApiVersion || 1);
    };

    it(`should list 0 project's achievements before adding new`, async () => {
      await request(app.getHttpServer())
        .get(`/project/${projectId}/achievement`)
        .set('Authorization', `Bearer ${token}`)
        .expect(200)
        .expect(res => {
          expect(res.body.length).toBe(0);
        });
    });

    it(`should add new achievement definition`, async () => {
      const dto = getDto();
      await request(app.getHttpServer())
        .post(`/project/${projectId}/achievement`)
        .set('Authorization', `Bearer ${token}`)
        .send(dto)
        .expect(201)
        .expect(testAchievementResponse(dto))
        .expect(res => {
          achievementId = res.body[0].id;
          expect(res.body[0].availableInGroups.length).toBe(dto.availableInGroups.length);
          expect(res.body[0].availableInGroups[0].id).toBe(dto.availableInGroups[0]);
        });
    });

    it(`should list 1 project's achievement after adding 1`, async () => {
      await request(app.getHttpServer())
        .get(`/project/${projectId}/achievement`)
        .set('Authorization', `Bearer ${token}`)
        .expect(200)
        .expect(res => {
          expect(res.body.length).toBe(1);
        });
    });

    it(`should modify achievement definition`, async () => {
      await testUpdate(
        req => req.put(`/project/${projectId}/achievement/${achievementId}`).set('Authorization', `Bearer ${token}`),
        testAchievementResponse,
        getDto(),
        { name: 'New name' },
        { name: 'Other name', description: 'Changed description' },
        { description: 'Other description' }
      );
    });

    it(`should delete achievement definition`, async () => {
      let id;
      await request(app.getHttpServer())
        .post(`/project/${projectId}/achievement`)
        .set('Authorization', `Bearer ${token}`)
        .send(getDto())
        .expect(201)
        .expect(res => {
          id = res.body[0].id;
        });

      await request(app.getHttpServer())
        .delete(`/project/${projectId}/achievement/${id}`)
        .set('Authorization', `Bearer ${token}`)
        .expect(200);
    });
  });

  describe('Achievements rules v2', () => {
    const getAchievementDto: () => AchievementDto = () => ({
      name: 'Half way through!',
      image: 'http://test.com/image.png',
      description: 'Requires score >= 50 in at least half of tasks in a group',
      scope: Scope.GROUP,
      availableInGroups: [groupIds[1]],
      triggeredBy: ['score'],
      rulesApiVersion: 2,
      rules: [
        {
          '>=': [
            {
              '/': [
                // liczba zadan z wyslanym zadaniem z wynikiem >= 50
                {
                  reduce: [
                    { var: 'tasksInGroup' },
                    {
                      '+': [
                        {
                          if: [
                            {
                              '>=': [{ var: ['current.lastEvent.data.score', 0] }, 50]
                            },
                            1,
                            0
                          ]
                        },
                        { var: 'accumulator' }
                      ]
                    },
                    0
                  ]
                },
                // liczba wszystkich zadan
                { var: 'tasksInGroup.length' }
              ]
            },
            0.5
          ]
        }
      ]
    });

    const testAchievementResponse = (expected: AchievementDto) => (res: any) => {
      expect(res.body.length).toBe(1);
      expect(res.body[0].id).toBeTruthy();
      expect(res.body[0].description).toBe(expected.description);
      expect(res.body[0].image).toBe(expected.image);
      expect(res.body[0].projectId).toBe(projectId);
      expect(res.body[0].scope).toBe(expected.scope);
      expect(res.body[0].rulesApiVersion).toBe(expected.rulesApiVersion);
    };

    it(`should add new achievement definition with rules api version 2`, async () => {
      const dto = getAchievementDto();
      await request(app.getHttpServer())
        .post(`/project/${projectId}/achievement`)
        .set('Authorization', `Bearer ${token}`)
        .send(dto)
        .expect(201)
        .expect(testAchievementResponse(dto))
        .expect(res => {
          achievementId = res.body[0].id;
          expect(res.body[0].availableInGroups.length).toBe(dto.availableInGroups.length);
          expect(res.body[0].availableInGroups[0].id).toBe(dto.availableInGroups[0]);
        });
    });
  });

  describe('Event', () => {
    jest.setTimeout(10000);
    it('sends event resulting in new achievement', async () => {
      const callbackQueue = module.get<CallbackQueue>(CallbackQueue);
      const callbackProcess = jest.spyOn(callbackQueue, '__process');

      await request(app.getHttpServer())
        .post(`/event/${projectId}`)
        .set('Authorization', `Bearer ${token}`)
        .send({
          actorId: '1',
          occurredAt: new Date(),
          taskId,
          data: {
            tries: 2,
            score: 100
          }
        } as EventDto)
        .expect(201);

      await waitForExpect(() => {
        expect(callbackProcess).toHaveBeenCalled();
      });
    });
  });
});
