import { INestApplication, Injectable, Logger, OnModuleDestroy, OnModuleInit, ValidationPipe } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import request from 'supertest';
import { AppModule } from '../src/modules/app.module';
import { IPGuard } from '../src/modules/auth/ip.guard';
import { UserModule } from '../src/modules/user/user.module';
import { UserService } from '../src/modules/user/user.service';
import { getConnection } from 'typeorm';

@Injectable()
export class TestHelper implements OnModuleInit, OnModuleDestroy {
  constructor(private userService: UserService) {}

  async onModuleInit(): Promise<void> {
    await getConnection().synchronize(true);
    await this.seedDemoUsers();
  }

  onModuleDestroy(): void {}

  private async seedDemoUsers(): Promise<void> {
    try {
      const user = await this.userService.create({
        login: 'demoUser',
        password: 'pass'
      });
      await this.userService.save(user);
    } catch (e) {}
  }
}

export async function initializeApplication(): Promise<[INestApplication, TestingModule]> {
  const moduleFixture = await Test.createTestingModule({
    imports: [AppModule, UserModule],
    providers: [TestHelper]
  }).compile();

  const app = moduleFixture.createNestApplication(undefined, {
    logger: Logger
  });
  app.useGlobalPipes(new ValidationPipe());

  await app.init();
  return [app, moduleFixture];
}

export async function logInDemoUser(app: INestApplication): Promise<string> {
  return new Promise<string>((resolve, reject) => {
    request(app.getHttpServer())
      .post('/auth/login')
      .send({ username: 'demoUser', password: 'pass' })
      .expect(201)
      .end((err, res) => {
        if (err) {
          reject(err);
        } else {
          resolve(res.body.token);
        }
      });
  });
}
