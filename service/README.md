# Gratification Service


## Installation

```bash
$ npm install
```

## Running the app

```bash
$ npm run start
```

### To configure the app make sure that following ENV variables are set
```bash
// VARIABLE=EXAMPLE
DB_HOST=localhost // Database host 
DB_PORT=5432 // Database host 
DB_USER=user // Database host 
DB_PASS=pass // Database host 
DATABASE=gratification-service // Database host

// or whole URL (will overwrite previous configuration)
DATABASE_URL=postgres://USERNAME:PASSWORD@HOST:PORT/DATABASE
```

## Test

```bash
$ npm run test
```

