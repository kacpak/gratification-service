#!/bin/sh
# $1 is migration name

if [ -z "$*" ]; then echo "No migration name provided"; exit 1; fi

docker-compose down
docker-compose up -d db

until [[ "`docker inspect -f {{.State.Health.Status}} gratification-service_db_1`"=="healthy" ]]; do
    sleep 0.1;
done;

npm run build
npm run migration:run
npm run migration:generate $1
npm run build
npm run migration:run
