module.exports = {
  "moduleFileExtensions": ["js", "json", "ts"],
  "rootDir": "test",
  "testRegex": ".spec.ts$",
  "transform": {
    "^.+\\.(t|j)s$": "ts-jest"
  },
  "verbose": true,
  "reporters": [
    "default",
    [ "jest-junit", { "output": "build/junit.xml" } ]
  ]
};
