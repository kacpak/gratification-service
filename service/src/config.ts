import { logger } from './logger';

interface DatabaseConfig {
  host: string;
  port: number;
  username: string;
  password: string;
  database: string;
}

interface Config {
  db: DatabaseConfig;
  port: number;
}

function getDatabaseEnvironment(): DatabaseConfig {
  // Local environment
  let host: string = process.env.DB_HOST || 'localhost';
  let port: number = parseInt(process.env.DB_PORT) || 5432;
  let username: string = process.env.DB_USER || 'user';
  let password: string = process.env.DB_PASS || 'pass';
  let database: string = process.env.DATABASE || 'gratification-service';

  if (process.env.DATABASE_URL) {
    let _, portString;
    [_, username, password, host, portString, database] = /postgres:\/\/(.+):(.+)@(.+):(.+)\/(.+)/.exec(process.env.DATABASE_URL);
    port = parseInt(portString, 10);
  }

  const config = {
    host,
    port,
    username,
    password,
    database
  };

  if (process.env.NODE_ENV === 'development') {
    logger.log(`Configuration: ${JSON.stringify(config)}`, 'Config');
  }
  return config;
}

function prepareEnvironment(): Config {
  const port = process.env.PORT ? parseInt(process.env.PORT, 10) : 4000;
  return {
    db: getDatabaseEnvironment(),
    port
  };
}

export const config = prepareEnvironment();
