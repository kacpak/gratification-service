if (!process.env.NODE_ENV) {
  process.env.NODE_ENV = 'production';
}

import { ValidationPipe } from '@nestjs/common';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppModule } from './modules/app.module';
import { config } from './config';
import { IPGuard } from './modules/auth/ip.guard';
import seed from './seed';
import * as path from 'path';
import express from 'express';
import * as http from 'http';
import { NestFactoryStatic } from '@nestjs/core/nest-factory';
import helmet from 'helmet'
import { logger } from "./logger";

async function bootstrap() {
  logger.log(process.env.NODE_ENV, 'NODE_ENV');
  const server = express();

  if (process.env.GRATIFICATION_SERVICE_SERVE_CLIENT_SIDE !== 'false') {
    const root = path.join(__dirname, 'client');
    server.use(express.static(root));
  }

  const appFactory = new NestFactoryStatic();
  const app = await appFactory.create(AppModule, server, {
    logger
  });
  app.setGlobalPrefix('api');
  app.enableCors({
    origin: process.env.GRATIFICATION_SERVICE_CORS_ORIGIN || '*',
    allowedHeaders: process.env.GRATIFICATION_SERVICE_CORS_ALLOWED_HEADERS || ['Authorization']
  });

  const ipWhitelist = (process.env.GRATIFICATION_SERVICE_IP_WHITELIST || '')
    .trim()
    .split(',')
    .map(addr => addr.trim())
    .filter(Boolean);

  if (ipWhitelist.length) {
    logger.log(`IP addresses whitelist created: ${ipWhitelist.join(', ')}`, 'Main');
  } else {
    logger.log('No IP addresses whitelist specified', 'Main');
  }
  app.useGlobalGuards(new IPGuard(ipWhitelist, logger));
  app.useGlobalPipes(new ValidationPipe());
  app.use(helmet());

  const swaggerOptions = new DocumentBuilder()
    .setTitle('Gratification Service')
    .setDescription('Gratification Service Public API description')
    .setVersion('1.0')
    .setBasePath('/api')
    .addTag('Authentication', 'Authentication stuff')
    .addTag('User')
    .addTag('Project', 'Project management (Groups, Tasks, Achievements)')
    .addTag('Event', 'Sending events to evaluate data from')
    .addBearerAuth('Authorization', 'header')
    .build();
  const document = SwaggerModule.createDocument(app, swaggerOptions);
  SwaggerModule.setup('docs', app, document);

  await app.init();

  // if (process.env.NODE_ENV === 'development') {
  //   logger.log('Started seeding.', 'Seeding');
  //   await seed(true);
  //   logger.log('Seeding done.', 'Seeding');
  // }

  http.createServer(server).listen(config.port, () => {
    logger.log(`Service available at: http://localhost:${config.port}`, 'Init');
  });
}

bootstrap().catch(e => {
  logger.error(e);
  process.exit(1);
});
