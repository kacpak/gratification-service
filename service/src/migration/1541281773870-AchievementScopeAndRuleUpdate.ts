import { MigrationInterface, QueryRunner } from 'typeorm';

export class AchievementScopeAndRuleUpdate1541281773870 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DROP INDEX "IDX_6dc8b7a0c3a3e0a42737c15578"`);
    await queryRunner.query(`ALTER TABLE "achievement" RENAME COLUMN "continuous" TO "scope"`);
    await queryRunner.query(
      `CREATE TABLE "group_assigned_achievements_achievement" ("groupId" uuid NOT NULL, "achievementId" uuid NOT NULL, CONSTRAINT "PK_1674090a3b98b20f5cf115ce2d5" PRIMARY KEY ("groupId", "achievementId"))`
    );
    await queryRunner.query(`ALTER TABLE "acquired_achievement" ADD "forGroupId" uuid`);
    await queryRunner.query(`ALTER TABLE "acquired_achievement" ADD "forTaskId" uuid`);
    await queryRunner.query(`ALTER TABLE "token" ADD CONSTRAINT "UQ_d9959ee7e17e2293893444ea371" UNIQUE ("token")`);
    await queryRunner.query(`ALTER TABLE "event" DROP CONSTRAINT "FK_56b239fecbe385af3a3186acab0"`);
    await queryRunner.query(`DROP INDEX "IDX_56b239fecbe385af3a3186acab"`);
    await queryRunner.query(`ALTER TABLE "event" DROP COLUMN "taskId"`);
    await queryRunner.query(`ALTER TABLE "event" ADD "taskId" uuid NOT NULL`);
    await queryRunner.query(`ALTER TABLE "task" DROP CONSTRAINT "PK_fb213f79ee45060ba925ecd576e"`);
    await queryRunner.query(`ALTER TABLE "task" DROP COLUMN "id"`);
    await queryRunner.query(`ALTER TABLE "task" ADD "id" uuid NOT NULL DEFAULT uuid_generate_v4()`);
    await queryRunner.query(`ALTER TABLE "task" ADD CONSTRAINT "PK_fb213f79ee45060ba925ecd576e" PRIMARY KEY ("id")`);
    await queryRunner.query(`ALTER TABLE "achievement" DROP COLUMN "scope"`);
    await queryRunner.query(`ALTER TABLE "achievement" ADD "scope" integer NOT NULL`);
    await queryRunner.query(
      `CREATE UNIQUE INDEX "IDX_3ab53d066ce76d45eb627e93a1" ON "acquired_achievement"("actorId", "achievementId", "forGroupId") `
    );
    await queryRunner.query(
      `CREATE UNIQUE INDEX "IDX_2447c37e8717baab79981effb2" ON "acquired_achievement"("actorId", "achievementId", "forTaskId") `
    );
    await queryRunner.query(`CREATE INDEX "IDX_56b239fecbe385af3a3186acab" ON "event"("taskId") `);
    await queryRunner.query(
      `ALTER TABLE "acquired_achievement" ADD CONSTRAINT "FK_2f6e6751a166fd2decb9de0d65c" FOREIGN KEY ("forGroupId") REFERENCES "group"("id")`
    );
    await queryRunner.query(
      `ALTER TABLE "acquired_achievement" ADD CONSTRAINT "FK_8ba60cfb607c44e48c8045bd8c2" FOREIGN KEY ("forTaskId") REFERENCES "task"("id")`
    );
    await queryRunner.query(
      `ALTER TABLE "event" ADD CONSTRAINT "FK_56b239fecbe385af3a3186acab0" FOREIGN KEY ("taskId") REFERENCES "task"("id")`
    );
    await queryRunner.query(
      `ALTER TABLE "group_assigned_achievements_achievement" ADD CONSTRAINT "FK_a9f143c223fd676c6d1753968d5" FOREIGN KEY ("groupId") REFERENCES "group"("id") ON DELETE CASCADE`
    );
    await queryRunner.query(
      `ALTER TABLE "group_assigned_achievements_achievement" ADD CONSTRAINT "FK_e70733b32ccdbc600d317cdbd10" FOREIGN KEY ("achievementId") REFERENCES "achievement"("id") ON DELETE CASCADE`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "group_assigned_achievements_achievement" DROP CONSTRAINT "FK_e70733b32ccdbc600d317cdbd10"`);
    await queryRunner.query(`ALTER TABLE "group_assigned_achievements_achievement" DROP CONSTRAINT "FK_a9f143c223fd676c6d1753968d5"`);
    await queryRunner.query(`ALTER TABLE "event" DROP CONSTRAINT "FK_56b239fecbe385af3a3186acab0"`);
    await queryRunner.query(`ALTER TABLE "acquired_achievement" DROP CONSTRAINT "FK_8ba60cfb607c44e48c8045bd8c2"`);
    await queryRunner.query(`ALTER TABLE "acquired_achievement" DROP CONSTRAINT "FK_2f6e6751a166fd2decb9de0d65c"`);
    await queryRunner.query(`DROP INDEX "IDX_56b239fecbe385af3a3186acab"`);
    await queryRunner.query(`DROP INDEX "IDX_2447c37e8717baab79981effb2"`);
    await queryRunner.query(`DROP INDEX "IDX_3ab53d066ce76d45eb627e93a1"`);
    await queryRunner.query(`ALTER TABLE "achievement" DROP COLUMN "scope"`);
    await queryRunner.query(`ALTER TABLE "achievement" ADD "scope" boolean NOT NULL`);
    await queryRunner.query(`ALTER TABLE "task" DROP CONSTRAINT "PK_fb213f79ee45060ba925ecd576e"`);
    await queryRunner.query(`ALTER TABLE "task" DROP COLUMN "id"`);
    await queryRunner.query(`ALTER TABLE "task" ADD "id" SERIAL NOT NULL`);
    await queryRunner.query(`ALTER TABLE "task" ADD CONSTRAINT "PK_fb213f79ee45060ba925ecd576e" PRIMARY KEY ("id")`);
    await queryRunner.query(`ALTER TABLE "event" DROP COLUMN "taskId"`);
    await queryRunner.query(`ALTER TABLE "event" ADD "taskId" integer NOT NULL`);
    await queryRunner.query(`CREATE INDEX "IDX_56b239fecbe385af3a3186acab" ON "event"("taskId") `);
    await queryRunner.query(
      `ALTER TABLE "event" ADD CONSTRAINT "FK_56b239fecbe385af3a3186acab0" FOREIGN KEY ("taskId") REFERENCES "task"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
    await queryRunner.query(`ALTER TABLE "token" DROP CONSTRAINT "UQ_d9959ee7e17e2293893444ea371"`);
    await queryRunner.query(`ALTER TABLE "acquired_achievement" DROP COLUMN "forTaskId"`);
    await queryRunner.query(`ALTER TABLE "acquired_achievement" DROP COLUMN "forGroupId"`);
    await queryRunner.query(`DROP TABLE "group_assigned_achievements_achievement"`);
    await queryRunner.query(`ALTER TABLE "achievement" RENAME COLUMN "scope" TO "continuous"`);
    await queryRunner.query(`CREATE UNIQUE INDEX "IDX_6dc8b7a0c3a3e0a42737c15578" ON "acquired_achievement"("actorId", "achievementId") `);
  }
}
