import {MigrationInterface, QueryRunner} from "typeorm";

export class IntroduceRulesApiVersion1614459098493 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "achievement" ADD "rulesApiVersion" integer NOT NULL DEFAULT 1`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "achievement" DROP COLUMN "rulesApiVersion"`);
    }

}
