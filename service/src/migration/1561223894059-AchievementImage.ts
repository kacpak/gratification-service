import { MigrationInterface, QueryRunner } from 'typeorm';

export class AchievementImage1561223894059 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "achievement" ADD "image" text NOT NULL DEFAULT ''`);
    await queryRunner.query(`ALTER TABLE "group" DROP CONSTRAINT "FK_eab1ac7e29cfe9e26fc99f6926c"`);
    await queryRunner.query(`ALTER TABLE "group" ALTER COLUMN "projectId" SET NOT NULL`);
    await queryRunner.query(`ALTER TABLE "achievement" ALTER COLUMN "scope" SET DEFAULT 1`);
    await queryRunner.query(`CREATE INDEX "IDX_79daf0d2be103f4c30c77ddd6b" ON "user_projects_project" ("userId") `);
    await queryRunner.query(`CREATE INDEX "IDX_936561888bfd63d01c79fe415c" ON "user_projects_project" ("projectId") `);
    await queryRunner.query(`CREATE INDEX "IDX_a9f143c223fd676c6d1753968d" ON "group_assigned_achievements_achievement" ("groupId") `);
    await queryRunner.query(
      `CREATE INDEX "IDX_e70733b32ccdbc600d317cdbd1" ON "group_assigned_achievements_achievement" ("achievementId") `
    );
    await queryRunner.query(
      `ALTER TABLE "group" ADD CONSTRAINT "FK_eab1ac7e29cfe9e26fc99f6926c" FOREIGN KEY ("projectId") REFERENCES "project"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "group" DROP CONSTRAINT "FK_eab1ac7e29cfe9e26fc99f6926c"`);
    await queryRunner.query(`DROP INDEX "IDX_e70733b32ccdbc600d317cdbd1"`);
    await queryRunner.query(`DROP INDEX "IDX_a9f143c223fd676c6d1753968d"`);
    await queryRunner.query(`DROP INDEX "IDX_936561888bfd63d01c79fe415c"`);
    await queryRunner.query(`DROP INDEX "IDX_79daf0d2be103f4c30c77ddd6b"`);
    await queryRunner.query(`ALTER TABLE "achievement" ALTER COLUMN "scope" DROP DEFAULT`);
    await queryRunner.query(`ALTER TABLE "group" ALTER COLUMN "projectId" DROP NOT NULL`);
    await queryRunner.query(
      `ALTER TABLE "group" ADD CONSTRAINT "FK_eab1ac7e29cfe9e26fc99f6926c" FOREIGN KEY ("projectId") REFERENCES "project"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
    await queryRunner.query(`ALTER TABLE "achievement" DROP COLUMN "image"`);
  }
}
