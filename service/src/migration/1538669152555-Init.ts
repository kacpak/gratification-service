import { MigrationInterface, QueryRunner } from 'typeorm';

export class Init1538669152555 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE TABLE "token" ("token" character varying NOT NULL, "userAgent" character varying NOT NULL, "createdDate" TIMESTAMP NOT NULL DEFAULT now(), "userId" uuid, CONSTRAINT "UQ_d9959ee7e17e2293893444ea371" UNIQUE ("token"), CONSTRAINT "PK_d9959ee7e17e2293893444ea371" PRIMARY KEY ("token"))`
    );
    await queryRunner.query(
      `CREATE TABLE "user" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "login" character varying(128) NOT NULL, "password" character varying NOT NULL, CONSTRAINT "UQ_a62473490b3e4578fd683235c5e" UNIQUE ("login"), CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY ("id"))`
    );
    await queryRunner.query(
      `CREATE TABLE "acquired_achievement" ("id" SERIAL NOT NULL, "actorId" integer NOT NULL, "achievementId" uuid NOT NULL, "level" integer NOT NULL, CONSTRAINT "PK_2f8eb0b4fa0b32075c34f27ccc2" PRIMARY KEY ("id"))`
    );
    await queryRunner.query(`CREATE UNIQUE INDEX "IDX_6dc8b7a0c3a3e0a42737c15578" ON "acquired_achievement"("actorId", "achievementId") `);
    await queryRunner.query(
      `CREATE TABLE "actor" ("id" SERIAL NOT NULL, "localId" character varying NOT NULL, "projectId" uuid NOT NULL, "hasAchievements" boolean NOT NULL DEFAULT true, CONSTRAINT "PK_05b325494fcc996a44ae6928e5e" PRIMARY KEY ("id"))`
    );
    await queryRunner.query(`CREATE UNIQUE INDEX "IDX_f63567006bf2541c53f986f8f0" ON "actor"("localId", "projectId") `);
    await queryRunner.query(
      `CREATE TABLE "event" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "type" character varying NOT NULL DEFAULT 'activity', "actorId" integer NOT NULL, "taskId" integer NOT NULL, "data" jsonb NOT NULL, "occurredAt" TIMESTAMP NOT NULL, CONSTRAINT "PK_30c2f3bbaf6d34a55f8ae6e4614" PRIMARY KEY ("id"))`
    );
    await queryRunner.query(`CREATE INDEX "IDX_b8c3aeeac35ace9d387fb5e142" ON "event"("type") `);
    await queryRunner.query(`CREATE INDEX "IDX_56b239fecbe385af3a3186acab" ON "event"("taskId") `);
    await queryRunner.query(
      `CREATE TABLE "task" ("id" SERIAL NOT NULL, "name" character varying(128) NOT NULL, "localId" character varying NOT NULL, "groupId" uuid NOT NULL, "meta" jsonb NOT NULL, "provides" jsonb NOT NULL, CONSTRAINT "PK_fb213f79ee45060ba925ecd576e" PRIMARY KEY ("id"))`
    );
    await queryRunner.query(`CREATE UNIQUE INDEX "IDX_72e4ce4cd41ffb6ba222ca4ded" ON "task"("localId", "groupId") `);
    await queryRunner.query(
      `CREATE TABLE "group" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "name" character varying(128) NOT NULL, "description" character varying, "projectId" uuid, CONSTRAINT "PK_256aa0fda9b1de1a73ee0b7106b" PRIMARY KEY ("id"))`
    );
    await queryRunner.query(
      `CREATE TABLE "project" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "name" character varying(128) NOT NULL, "callbackUrl" character varying, "isABTested" boolean NOT NULL DEFAULT false, CONSTRAINT "PK_4d68b1358bb5b766d3e78f32f57" PRIMARY KEY ("id"))`
    );
    await queryRunner.query(
      `CREATE TABLE "achievement" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "name" character varying NOT NULL, "description" character varying NOT NULL, "continuous" boolean NOT NULL, "triggeredBy" jsonb NOT NULL, "rules" jsonb NOT NULL, "projectId" uuid NOT NULL, CONSTRAINT "PK_441339f40e8ce717525a381671e" PRIMARY KEY ("id"))`
    );
    await queryRunner.query(
      `CREATE TABLE "user_projects_project" ("userId" uuid NOT NULL, "projectId" uuid NOT NULL, CONSTRAINT "PK_26a180af1ec7a8550f5c374fcd8" PRIMARY KEY ("userId", "projectId"))`
    );
    await queryRunner.query(
      `ALTER TABLE "token" ADD CONSTRAINT "FK_94f168faad896c0786646fa3d4a" FOREIGN KEY ("userId") REFERENCES "user"("id")`
    );
    await queryRunner.query(
      `ALTER TABLE "acquired_achievement" ADD CONSTRAINT "FK_dd44e1e484c7ee8d7b9a74ab6bc" FOREIGN KEY ("actorId") REFERENCES "actor"("id")`
    );
    await queryRunner.query(
      `ALTER TABLE "acquired_achievement" ADD CONSTRAINT "FK_1af53b771eb5aefad69fea18c2f" FOREIGN KEY ("achievementId") REFERENCES "achievement"("id")`
    );
    await queryRunner.query(
      `ALTER TABLE "actor" ADD CONSTRAINT "FK_95254d7f5303c308467c9353be2" FOREIGN KEY ("projectId") REFERENCES "project"("id")`
    );
    await queryRunner.query(
      `ALTER TABLE "event" ADD CONSTRAINT "FK_f0f4786ba5e7d53e5853b197f0e" FOREIGN KEY ("actorId") REFERENCES "actor"("id")`
    );
    await queryRunner.query(
      `ALTER TABLE "event" ADD CONSTRAINT "FK_56b239fecbe385af3a3186acab0" FOREIGN KEY ("taskId") REFERENCES "task"("id")`
    );
    await queryRunner.query(
      `ALTER TABLE "task" ADD CONSTRAINT "FK_b8e1728a46f2cbb7b937011ae4f" FOREIGN KEY ("groupId") REFERENCES "group"("id")`
    );
    await queryRunner.query(
      `ALTER TABLE "group" ADD CONSTRAINT "FK_eab1ac7e29cfe9e26fc99f6926c" FOREIGN KEY ("projectId") REFERENCES "project"("id")`
    );
    await queryRunner.query(
      `ALTER TABLE "achievement" ADD CONSTRAINT "FK_59097159b1900229eda60536df5" FOREIGN KEY ("projectId") REFERENCES "project"("id")`
    );
    await queryRunner.query(
      `ALTER TABLE "user_projects_project" ADD CONSTRAINT "FK_79daf0d2be103f4c30c77ddd6be" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE CASCADE`
    );
    await queryRunner.query(
      `ALTER TABLE "user_projects_project" ADD CONSTRAINT "FK_936561888bfd63d01c79fe415c3" FOREIGN KEY ("projectId") REFERENCES "project"("id") ON DELETE CASCADE`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "user_projects_project" DROP CONSTRAINT "FK_936561888bfd63d01c79fe415c3"`);
    await queryRunner.query(`ALTER TABLE "user_projects_project" DROP CONSTRAINT "FK_79daf0d2be103f4c30c77ddd6be"`);
    await queryRunner.query(`ALTER TABLE "achievement" DROP CONSTRAINT "FK_59097159b1900229eda60536df5"`);
    await queryRunner.query(`ALTER TABLE "group" DROP CONSTRAINT "FK_eab1ac7e29cfe9e26fc99f6926c"`);
    await queryRunner.query(`ALTER TABLE "task" DROP CONSTRAINT "FK_b8e1728a46f2cbb7b937011ae4f"`);
    await queryRunner.query(`ALTER TABLE "event" DROP CONSTRAINT "FK_56b239fecbe385af3a3186acab0"`);
    await queryRunner.query(`ALTER TABLE "event" DROP CONSTRAINT "FK_f0f4786ba5e7d53e5853b197f0e"`);
    await queryRunner.query(`ALTER TABLE "actor" DROP CONSTRAINT "FK_95254d7f5303c308467c9353be2"`);
    await queryRunner.query(`ALTER TABLE "acquired_achievement" DROP CONSTRAINT "FK_1af53b771eb5aefad69fea18c2f"`);
    await queryRunner.query(`ALTER TABLE "acquired_achievement" DROP CONSTRAINT "FK_dd44e1e484c7ee8d7b9a74ab6bc"`);
    await queryRunner.query(`ALTER TABLE "token" DROP CONSTRAINT "FK_94f168faad896c0786646fa3d4a"`);
    await queryRunner.query(`DROP TABLE "user_projects_project"`);
    await queryRunner.query(`DROP TABLE "achievement"`);
    await queryRunner.query(`DROP TABLE "project"`);
    await queryRunner.query(`DROP TABLE "group"`);
    await queryRunner.query(`DROP INDEX "IDX_72e4ce4cd41ffb6ba222ca4ded"`);
    await queryRunner.query(`DROP TABLE "task"`);
    await queryRunner.query(`DROP INDEX "IDX_56b239fecbe385af3a3186acab"`);
    await queryRunner.query(`DROP INDEX "IDX_b8c3aeeac35ace9d387fb5e142"`);
    await queryRunner.query(`DROP TABLE "event"`);
    await queryRunner.query(`DROP INDEX "IDX_f63567006bf2541c53f986f8f0"`);
    await queryRunner.query(`DROP TABLE "actor"`);
    await queryRunner.query(`DROP INDEX "IDX_6dc8b7a0c3a3e0a42737c15578"`);
    await queryRunner.query(`DROP TABLE "acquired_achievement"`);
    await queryRunner.query(`DROP TABLE "user"`);
    await queryRunner.query(`DROP TABLE "token"`);
  }
}
