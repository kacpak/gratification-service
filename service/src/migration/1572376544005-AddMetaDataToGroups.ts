import {MigrationInterface, QueryRunner} from "typeorm";

export class AddMetaDataToGroups1572376544005 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "group" ADD "meta" jsonb`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "group" DROP COLUMN "meta"`);
    }

}
