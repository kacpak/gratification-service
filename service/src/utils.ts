export function updateDefinedObjectKeys<T>(original: T, newValues: Partial<T>): T {
  const copy = Object.assign({}, original);
  for (const key of Object.keys(newValues)) {
    const newValue = newValues[key];
    if (newValue !== undefined) {
      copy[key] = newValue;
    }
  }
  return copy;
}
