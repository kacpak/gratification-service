import { Connection, Repository, getConnection } from 'typeorm';
import { Achievement, Scope } from './modules/api/achievements/achievement.entity';
import { Event } from './modules/api/event/event.entity';
import { User } from './modules/user/user.entity';
import { Project } from './modules/api/project/project.entity';
import { Group } from './modules/api/group/group.entity';
import { Task } from './modules/api/task/task.entity';
import { Token } from './modules/auth/token.entity';
import { Actor } from './modules/api/actor/actor.entity';
import * as bcrypt from 'bcrypt';
import { logger } from "./logger";

interface DateData {
  year?: number;
  month?: number;
  date?: number;
  hours?: number;
  minutes?: number;
  seconds?: number;
  ms?: number;
}

function addToDate(date: Date, add: DateData): Date {
  const diff: DateData = Object.assign(
    {
      year: 0,
      month: 0,
      date: 0,
      hours: 0,
      minutes: 0,
      seconds: 0,
      ms: 0
    } as DateData,
    add
  );
  return new Date(
    date.getFullYear() + diff.year,
    date.getMonth() + diff.month,
    date.getDate() + diff.date,
    date.getHours() + diff.hours,
    date.getMinutes() + diff.minutes,
    date.getSeconds() + diff.seconds,
    date.getMilliseconds() + diff.ms
  );
}

class Seeder {
  private readonly userRepository: Repository<User>;
  private readonly tokenRepository: Repository<Token>;
  private readonly projectRepository: Repository<Project>;
  private readonly groupRepository: Repository<Group>;
  private readonly taskRepository: Repository<Task>;
  private readonly achievementRepository: Repository<Achievement>;
  private readonly eventRepository: Repository<Event>;
  private readonly actorRepository: Repository<Actor>;

  constructor(private connection: Connection) {
    this.userRepository = connection.getRepository(User);
    this.tokenRepository = connection.getRepository(Token);
    this.projectRepository = connection.getRepository(Project);
    this.groupRepository = connection.getRepository(Group);
    this.taskRepository = connection.getRepository(Task);
    this.achievementRepository = connection.getRepository(Achievement);
    this.eventRepository = connection.getRepository(Event);
    this.actorRepository = connection.getRepository(Actor);
  }

  async seed(force: boolean = false) {
    await this.connection.synchronize(true);

    // const demoUserCount = await this.userRepository.count({
    //   where: {
    //     login: 'user'
    //   }
    // });
    //
    // if (!force && demoUserCount > 0) {
    //   logger.log('Seeding skipped');
    //   return;
    // }

    const user = this.userRepository.create({
      login: 'user',
      password: await bcrypt.hash('pass', 10)
    });
    await this.userRepository.save(user);

    const project = this.projectRepository.create({
      id: '474b1dd4-e0cc-48cc-87a7-e4432db9f060',
      name: 'Programming Class',
      isABTested: false,
      callbackUrl: 'https://webhook.site/08f83d6c-e96a-41b3-8277-c762c2b6bb52', // Test on https://webhook.site/#/08f83d6c-e96a-41b3-8277-c762c2b6bb52
      users: [user]
    });
    await this.projectRepository.save(project);

    const groupExams = this.groupRepository.create({
      id: '89ab58b6-7f6e-43dd-abab-dc4a481013a0',
      name: 'Exams',
      project
    });
    groupExams.meta = {
      internalLink: 'http://onet.pl'
    };
    const groupExercises = this.groupRepository.create({
      name: 'Exercises',
      project
    });
    await this.groupRepository.save([groupExams, groupExercises]);

    const taskExercise1 = this.taskRepository.create({
      localId: '1',
      name: 'Exercise #1 - Introduction',
      group: groupExercises,
      provides: ['score', 'tries', 'date']
    });
    // DeepPartial workaround
    taskExercise1.meta = {
      startsAt: new Date().toISOString(),
      endsAt: addToDate(new Date(), { date: 7 }).toISOString(),
      maxScore: '100'
    };

    const taskExercise2 = this.taskRepository.create({
      localId: '2',
      name: 'Exercise #2 - Classes',
      group: groupExercises,
      provides: ['score', 'tries', 'date']
    });
    // DeepPartial workaround
    taskExercise2.meta = {
      startsAt: new Date().toISOString(),
      endsAt: addToDate(new Date(), { date: 7 }).toISOString(),
      maxScore: '100'
    };

    const taskExam1 = this.taskRepository.create({
      localId: '8',
      name: 'Exam #1 - C# Basics',
      group: groupExams,
      provides: ['score']
    });
    // DeepPartial workaround
    taskExam1.meta = {
      startsAt: new Date().toISOString(),
      endsAt: addToDate(new Date(), { month: 2 }).toISOString(),
      maxScore: '100'
    };

    await this.taskRepository.save([taskExercise1, taskExercise2, taskExam1]);

    const achievementBestInThree = this.achievementRepository.create({
      project,
      name: 'Best in 3 tries',
      description: 'Given when achieved 100% in max 3 tries',
      scope: Scope.TASK,
      triggeredBy: ['score', 'tries'],
      availableInGroups: [groupExercises]
    });
    achievementBestInThree.rules = [
      {
        and: [
          {
            value: '$score',
            comparator: '==',
            target: '$maxScore'
          },
          {
            value: '$tries',
            comparator: '<=',
            target: '3'
          }
        ]
      }
    ];
    await this.achievementRepository.save(achievementBestInThree);

    const achievementCompletionist = this.achievementRepository.create({
      project,
      name: 'All perfect',
      description: 'Given when achieved 100% in all tasks in group',
      scope: Scope.GROUP,
      triggeredBy: ['score'],
      availableInGroups: [groupExercises]
    });
    achievementCompletionist.rules = [
      {
        all: {
          value: '$score',
          comparator: '==',
          target: '$maxScore'
        }
      }
    ];
    await this.achievementRepository.save(achievementCompletionist);

    const achievementFirstCompleted = this.achievementRepository.create({
      project,
      name: 'First perfect score in group',
      description: 'Given when achieved perfect score in first task in a group',
      scope: Scope.GROUP,
      triggeredBy: ['score'],
      availableInGroups: [groupExercises]
    });
    achievementFirstCompleted.rules = [
      {
        some: {
          value: '$score',
          comparator: '==',
          target: '$maxScore'
        }
      }
    ];
    await this.achievementRepository.save(achievementFirstCompleted);

    // const achievementCompletionist = this.achievementRepository.create({
    //   project,
    //   name: 'Completionist',
    //   description: 'Odznaka przyznawana gdy score dla wszystkich (lub części wszystkich zadań) przekroczy 80%',
    //   scope: Scope.GROUP,
    //   triggeredBy: ['score'],
    //   availableInGroups: [groupExercises]
    // });
    // achievementCompletionist.rules = [
    //   {
    //     value: ['min-value', { providerValue: 'score' }], // Wybiera najmniejszą wartość ze scope
    //     comparator: '>=',
    //     target: 0.8
    //   },
    //   {
    //     value: ['min-value', { providerValue: 'score', top: 0.9 }], // Wybiera najmniejszą wartość ze scope dla 90% najlepszych wyników
    //     comparator: '>=',
    //     target: 0.8
    //   },
    //   {
    //     value: ['min-value', { providerValue: 'score', top: 0.8 }], // Wybiera najmniejszą wartość ze scope dla 80% najlepszych wyników
    //     comparator: '>=',
    //     target: 0.8
    //   }
    // ];
    // await this.achievementRepository.save(achievementCompletionist);
    //
    // const achievementEarlyBird = this.achievementRepository.create({
    //   project,
    //   name: 'Early Bird',
    //   description: '',
    //   scope: Scope.TASK,
    //   triggeredBy: ['occurredAt'],
    //   availableInGroups: [groupExercises]
    // });
    // achievementEarlyBird.rules = [
    //   {
    //     value: [
    //       'timediff',
    //       {
    //         start: '$startsAt',
    //         end: '$occurredAt',
    //         in: 'days'
    //       }
    //     ],
    //     comparator: '<',
    //     target: 1
    //   }
    // ];
    // await this.achievementRepository.save(achievementEarlyBird);

    const actor1 = this.actorRepository.create({
      project,
      localId: '2',
      hasAchievements: true
    });

    const actor2 = this.actorRepository.create({
      project,
      localId: '5',
      hasAchievements: false
    });

    await this.actorRepository.save([actor1, actor2]);

    const event1 = this.eventRepository.create({
      task: taskExercise1,
      occurredAt: new Date().toISOString(),
      actor: actor1
    });
    event1.data = {
      score: 15,
      tries: 1
    };

    const event2 = this.eventRepository.create({
      task: taskExercise1,
      occurredAt: addToDate(new Date(), { hours: 1, minutes: 35 }).toISOString(),
      actor: actor1
    });
    event2.data = {
      score: 60,
      tries: 2
    };

    const event3 = this.eventRepository.create({
      task: taskExercise2,
      occurredAt: addToDate(new Date(), { hours: 2 }).toISOString(),
      actor: actor1
    });
    event3.data = {
      score: 95,
      tries: 1
    };

    const event4 = this.eventRepository.create({
      task: taskExercise2,
      occurredAt: new Date().toISOString(),
      actor: actor2
    });
    event4.data = {
      score: 30,
      tries: 1
    };

    await this.eventRepository.save([event1, event2, event3, event4]);
  }
}

export default async function seed(force: boolean = false) {
  const connection = await getConnection();
  const seeder = new Seeder(connection);
  return seeder.seed(force);
}
