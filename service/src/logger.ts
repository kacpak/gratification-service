import path from 'path';
import winston, { Logger, LoggerOptions } from 'winston';
import { LoggerService } from '@nestjs/common';
import 'winston-daily-rotate-file';

class WinstonLogger implements LoggerService {
  constructor(private readonly logger: Logger) { }

  public log(message: any, context?: string) {
    return this.logger.info(message, { context });
  }

  public error(message: any, trace?: string, context?: string): any {
    return this.logger.error(message, { trace, context });
  }

  public warn(message: any, context?: string): any {
    return this.logger.warn(message, { context });
  }

  public debug?(message: any, context?: string): any {
    return this.logger.debug(message, { context });
  }

  public verbose?(message: any, context?: string): any {
    return this.logger.verbose(message, { context });
  }
}

function createWinstonLogger(opts: LoggerOptions): LoggerService {
  const logger: winston.Logger = winston.createLogger(opts);

  return new WinstonLogger(logger);
}

export const logger = createWinstonLogger({
  transports: [
    new winston.transports.Console({
      format: winston.format.combine(
        winston.format.timestamp(),
        winston.format.ms(),
        winston.format.colorize({ all: true }),
        winston.format.printf(({ context, level, timestamp, message, ms, trace }) => {
          const tracePrint = trace ? `\n${trace}` : '';
          return `${level}: ${new Date(timestamp).toLocaleString()}\t [${context}] ${message} ${ms}${tracePrint}`;
        }),
      )
    }),
    new (winston.transports as any).DailyRotateFile({
      filename: 'full-%DATE%.log',
      dirname: path.join(process.cwd(), 'logs'),
      datePattern: 'YYYY-MM-DD',
      format: winston.format.combine(
        winston.format.timestamp(),
        winston.format.ms(),
        winston.format.logstash()
      )
    }),
    new winston.transports.File({
      level: 'error',
      filename: path.join(process.cwd(), 'logs/errors.log'),
      format: winston.format.combine(
        winston.format.timestamp(),
        winston.format.ms(),
        winston.format.logstash()
      )
    }),
    new winston.transports.File({
      filename: path.join(process.cwd(), 'logs/events.log'),
      format: winston.format.combine(
        winston.format((info, opts) => {
          if (info.context && ['event', 'callback'].some(_ => info.context.toLowerCase().includes(_))) {
            return info;
          }
          return false;
        })(),
        winston.format.timestamp(),
        winston.format.ms(),
        winston.format.logstash()
      )
    })
  ]
});
