import { Injectable } from '@nestjs/common';
import { ComparableValue } from '../../api/achievements/achievement.dto';
import { EventService } from '../../api/event/event.service';
import { Event } from '../../api/event/event.entity';
import { logger } from "../../../logger";

export type Operator = (data: OperatorData) => Promise<ComparableValue>;
export interface OperatorData<O = any> {
  event: Event;
  options: O;
}

@Injectable()
export class OperatorsService {
  private static readonly operators = {
    'min-value': require('./min-value'),
    timediff: require('./timediff')
  };

  constructor(readonly eventService: EventService) {}

  getOperator(operatorName: string): Operator {
    let operator;
    try {
      operator = OperatorsService.operators[operatorName].default;
    } catch (e) {
      throw new Error(`Couldn't find operator "${operatorName}"`);
    }
    if (!operator) {
      logger.log(`Couldn't find operator "${operatorName}"`, 'OperatorsService');
      return;
    }
    return (data: OperatorData) => operator(this, data);
  }
}
