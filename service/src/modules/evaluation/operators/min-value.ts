import { OperatorData, OperatorsService } from './operators.service';
import { Event } from '../../api/event/event.entity';
import { getConnection } from 'typeorm';
import { logger } from "../../../logger";

interface MinValueOptions {
  providerValue: string;
  top?: number;
}

export default async function(operatorsService: OperatorsService, { event, options }: OperatorData<MinValueOptions>) {
  const query = await operatorsService.eventService
    .getRepository()
    .createQueryBuilder('event')
    .select('DISTINCT ON ("event"."taskId") "event"."taskId", "event"."id", "event"."data", "event"."occurredAt", "event"."actorId"')
    .where({
      actorId: event.actorId
    })
    .orderBy('"event"."taskId"')
    .addOrderBy('"event"."occurredAt"', 'DESC')
    .getQueryAndParameters();
  const latestEvents: Event[] = await getConnection().query(...query);
  logger.log(`Found ${latestEvents.length} events to check`, 'Operator min-value');

  return latestEvents.reduce((min, event) => Math.min(event.data[options.providerValue], min), Infinity);
}
