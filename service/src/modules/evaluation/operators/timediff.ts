import { ComparableValue } from '../../api/achievements/achievement.dto';
import { getValue } from '../utils';
import { OperatorData, OperatorsService } from './operators.service';
import { DateTime, DurationUnit } from 'luxon';
import { logger } from "../../../logger";

interface TimeDiffOptions {
  start: string;
  end: string;
  in: DurationUnit;
}

const log = message => logger.log(message, 'TimeDiff Operator');

export default async function(operatorsService: OperatorsService, data: OperatorData<TimeDiffOptions>): Promise<ComparableValue> {
  const start = DateTime.fromISO(getValue(data.event, data.options.start));
  const end = DateTime.fromISO(getValue(data.event, data.options.end));

  return end.diff(start, data.options.in).as(data.options.in || 'milliseconds');
}
