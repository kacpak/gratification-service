import { forwardRef, Module } from '@nestjs/common';
import { ApiModule } from '../api/api.module';
import { OperatorsService } from './operators/operators.service';
import { ComparatorsService } from './comparators/comparators.service';

@Module({
  imports: [forwardRef(() => ApiModule)],
  providers: [OperatorsService, ComparatorsService],
  exports: [OperatorsService, ComparatorsService]
})
export class EvaluationModule {}
