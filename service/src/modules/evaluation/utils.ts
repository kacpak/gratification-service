import { ComparableValue } from '../api/achievements/achievement.dto';
import { Event } from '../api/event/event.entity';

export function getValue(event: Event, value: ComparableValue) {
  if (typeof value === 'string' && value.startsWith('$')) {
    const variable = value.slice(1);

    const specialVariables = {
      actorId: event.actorId,
      projectId: event.actor.projectId,
      groupId: event.task.groupId,
      taskId: event.taskId,
      taskLocalId: event.task.localId,
      taskName: event.task.name,
      occurredAt: event.occurredAt
    };

    const variableFromEvent = event.data && event.data[variable];
    const variableFromTask = event.task.meta && event.task.meta[variable];
    const variableFromGroup = event.task.group.meta && event.task.group.meta[variable];

    return specialVariables[variable] || variableFromEvent || variableFromTask || variableFromGroup || undefined;
  }
  return value;
}
