import { Injectable } from '@nestjs/common';
import { ComparableValue } from '../../api/achievements/achievement.dto';
import { logger } from "../../../logger";

export type Comparator = (left: ComparableValue, right: ComparableValue) => boolean;

function castRight(left, right) {
  if (typeof left === 'number' && typeof right === 'string') {
    return parseInt(right, 10);
  } else if (typeof left === 'boolean') {
    return !!right;
  }
  return right;
}

function number(x: number | string): number {
  return parseInt(x as string, 10);
}

function string(x: number | string): string {
  if (typeof x === 'number') {
    return x.toString(10);
  }
  return x;
}

function float(x: number | string): number {
  return parseFloat(x as string);
}

const basicComparators = {
  '>=': (left, right) => left >= castRight(left, right),
  '>': (left, right) => left > castRight(left, right),
  '<=': (left, right) => left <= castRight(left, right),
  '<': (left, right) => left < castRight(left, right),
  '==': (left, right) => left === castRight(left, right)
};

const getTypedComparator = (prefix: string, func) =>
  Object.keys(basicComparators).reduce(
    (comparators, key) => Object.assign(comparators, { [`${prefix}${key}`]: (l, r) => basicComparators[key](func(l), func(r)) }),
    {}
  );

@Injectable()
export class ComparatorsService {
  static readonly comparators: { [key: string]: Comparator } = {
    ...basicComparators,
    ...getTypedComparator('n', number),
    ...getTypedComparator('s', string),
    ...getTypedComparator('f', float)
  };

  getComparator(comparatorName: string) {
    const comparator = ComparatorsService.comparators[comparatorName];
    if (!comparator) {
      logger.log(`Couldn't find comparator "${comparatorName}"`, 'ComparatorsService');
      return;
    }
    return comparator;
  }
}
