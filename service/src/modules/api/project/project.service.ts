import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, FindConditions, ObjectID, Repository } from 'typeorm';
import { Project } from './project.entity';
import { User } from '../../user/user.entity';
import { UserService } from '../../user/user.service';

@Injectable()
export class ProjectService {
  constructor(
    @InjectRepository(Project) private readonly projectRepository: Repository<Project>,
    private readonly userService: UserService
  ) {}

  async createForUser(project: Record<'users', User[]> & Partial<Project>): Promise<Project> {
    const newProject = this.projectRepository.create(project);
    return await this.projectRepository.save(newProject);
  }

  async findById(id: string): Promise<Project> {
    return this.projectRepository.findOneOrFail(id);
  }

  async findAllOfUser(user: Partial<User>): Promise<Project[]> {
    const userWithProjects = await this.userService.findWithProjects(user);
    return userWithProjects.projects;
  }

  async save(...projects: Project[]): Promise<Project[]> {
    return await this.projectRepository.save(projects);
  }

  async delete(
    criteria: string | string[] | number | number[] | Date | Date[] | ObjectID | ObjectID[] | FindConditions<Project>
  ): Promise<DeleteResult> {
    return await this.projectRepository.delete(criteria);
  }
}
