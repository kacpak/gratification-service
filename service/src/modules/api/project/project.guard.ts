import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { UserService } from '../../user/user.service';
import { logger } from "../../../logger";

@Injectable()
export class ProjectAccessGuard implements CanActivate {
  constructor(private readonly userService: UserService) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();
    const { projectId } = request.params;
    if (!projectId) {
      return true;
    }
    if (!request.user) {
      logger.log('No user object found during evaluation of project access', 'ProjectAccessGuard');
      return false;
    }
    const userWithProjects = await this.userService.findWithProjects({ id: request.user.id });
    return userWithProjects.projects.some(project => project.id === projectId);
  }
}
