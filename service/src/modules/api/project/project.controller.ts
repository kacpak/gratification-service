import { Get, Controller, UseGuards, Req, Post, Body, Param, Put, UsePipes, ValidationPipe, Delete } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiUseTags, ApiModelProperty } from '@nestjs/swagger';
import { IsBoolean, IsOptional, IsString, IsUrl } from 'class-validator';
import { updateDefinedObjectKeys } from '../../../utils';
import { Project } from './project.entity';
import { ProjectAccessGuard } from './project.guard';
import { ProjectService } from './project.service';
import { logger } from "../../../logger";

export class NewProjectDTO {
  @ApiModelProperty({
    description: 'project name',
    required: true,
    example: 'Basics of C# in object oriented programming'
  })
  @IsOptional({ groups: ['update'] })
  @IsString()
  name: string;

  @ApiModelProperty({
    required: true,
    description: `when 'true' all new users will be randomly selected to either have achievements or be ignored for randomized A/B testing`,
    example: 'false'
  })
  @IsOptional({ groups: ['update'] })
  @IsBoolean()
  isABTested: boolean;

  @ApiModelProperty({
    description: 'url to hit after achievement was acquired',
    required: false,
    example: 'https://www.webhook.site/ad53c157-2a30-4638-9c11-9af9948946bb'
  })
  @IsOptional({ groups: ['update'] })
  @IsUrl()
  callbackUrl: string;
}

@Controller('project')
@UseGuards(ProjectAccessGuard)
@UseGuards(AuthGuard('bearer'))
@ApiUseTags('Project')
@ApiBearerAuth()
@ApiResponse({ status: 401, description: 'Not authorized.' })
export class ProjectController {
  constructor(private readonly projectService: ProjectService) {}

  @Get('/')
  @ApiOperation({ title: 'List all projects of current user' })
  async getUserProjects(@Req() request): Promise<Project[]> {
    return await this.projectService.findAllOfUser(request.user);
  }

  @Get('/:projectId')
  @ApiOperation({ title: 'Get given project of current user' })
  async getUserProjectById(@Req() request, @Param('projectId') id: string): Promise<Project> {
    return await this.projectService.findById(id);
  }

  @Post('/')
  @UsePipes(new ValidationPipe({ groups: ['create'] }))
  @ApiOperation({ title: 'Create new project for current user' })
  async createNewProject(@Req() request, @Body() newProjectDTO: NewProjectDTO) {
    const res = await this.projectService.createForUser({
      name: newProjectDTO.name,
      callbackUrl: newProjectDTO.callbackUrl,
      isABTested: newProjectDTO.isABTested || false,
      users: [request.user]
    });
    logger.log(`Created new project: ${JSON.stringify(res)}`, 'ProjectController');
    return res;
  }

  @Put('/:projectId')
  @UsePipes(new ValidationPipe({ groups: ['update'] }))
  @ApiOperation({ title: 'Modify project' })
  async updateProject(@Body() newProjectDTO: NewProjectDTO, @Param('projectId') id: string) {
    const project = await this.projectService.findById(id);
    const updated = updateDefinedObjectKeys(project, {
      name: newProjectDTO.name,
      isABTested: newProjectDTO.isABTested,
      callbackUrl: newProjectDTO.callbackUrl
    });
    const res = await this.projectService.save(updated);
    logger.log(`Modified project: ${JSON.stringify(res)}`, 'ProjectController');
    return res;
  }

  @Delete('/:projectId')
  @ApiOperation({ title: 'Delete project' })
  async deleteTask(@Param('projectId') id: string) {
    const res = await this.projectService.delete({
      id
    });
    logger.log(`Deleted project "${id}"`, 'ProjectController');
    return res;
  }
}
