import { Entity, Column, PrimaryGeneratedColumn, ManyToMany, OneToMany } from 'typeorm';
import { User } from '../../user/user.entity';
import { Achievement } from '../achievements/achievement.entity';
import { Group } from '../group/group.entity';
import { Actor } from '../actor/actor.entity';

@Entity()
export class Project {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ length: 128 })
  name: string;

  @ManyToMany(type => User, user => user.projects)
  users: User[];

  @OneToMany(type => Group, group => group.project, { onDelete: 'CASCADE' })
  groups: Group[];

  @OneToMany(type => Achievement, achievement => achievement.project, { onDelete: 'CASCADE' })
  achievements: Achievement[];

  @OneToMany(type => Actor, actor => actor.project, { onDelete: 'CASCADE' })
  actors: Actor[];

  @Column({ nullable: true })
  callbackUrl: string;

  @Column({ default: false })
  isABTested: boolean;
}
