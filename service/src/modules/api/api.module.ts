import { forwardRef, Module } from '@nestjs/common';
import { AuthModule } from '../auth/auth.module';
import { AchievementController } from './achievements/achievement.controller';
import { Achievement } from './achievements/achievement.entity';
import { AchievementService } from './achievements/achievement.service';
import { AcquiredAchievement } from './achievements/acquiredAchievement.entity';
import { EventProcessor } from './event/event.processor';
import { Group } from './group/group.entity';
import { ProjectAccessGuard } from './project/project.guard';
import { Task } from './task/task.entity';
import { UserModule } from '../user/user.module';
import { ProjectController } from './project/project.controller';
import { ProjectService } from './project/project.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Project } from './project/project.entity';
import { EventController } from './event/event.controller';
import { Event } from './event/event.entity';
import { EventQueue } from './event/event.queue';
import { EventService } from './event/event.service';
import { GroupService } from './group/group.service';
import { GroupController } from './group/group.controller';
import { TaskController } from './task/task.controller';
import { TaskService } from './task/task.service';
import { ActorService } from './actor/actor.service';
import { Actor } from './actor/actor.entity';
import { EvaluationModule } from '../evaluation/evaluation.module';
import { ActorController } from './actor/actor.controller';
import { CallbackQueue } from './event/callback.queue';

@Module({
  imports: [
    AuthModule,
    UserModule,
    TypeOrmModule.forFeature([Project, Group, Task, Event, Achievement, Actor, AcquiredAchievement]),
    forwardRef(() => EvaluationModule)
  ],
  controllers: [ProjectController, EventController, GroupController, TaskController, AchievementController, ActorController],
  providers: [
    ProjectService,
    EventQueue,
    EventService,
    EventProcessor,
    GroupService,
    TaskService,
    AchievementService,
    ActorService,
    CallbackQueue,
    ProjectAccessGuard
  ],
  exports: [ProjectService, EventService, GroupService, TaskService, AchievementService, ActorService]
})
export class ApiModule {}
