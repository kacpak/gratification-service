import { ApiModelProperty } from '@nestjs/swagger';
import { ArrayNotEmpty, IsArray, IsIn, IsNotEmpty, IsNumber, IsOptional, IsString, ValidateNested } from 'class-validator';
import { ComparatorsService } from '../../evaluation/comparators/comparators.service';
import { RulesLogic } from 'json-logic-js';

export type ProviderValue = string;
export type ComparableValue = number | string | boolean;
export type RuleValue = string | [string, any?];

/**
 * General Rules
 */
export type RuleDefinition = RuleAndDefinition | RuleOrDefinition | Rule;
export class RuleAndDefinition {
  and: RuleDefinition[];
}
export class RuleOrDefinition {
  or: RuleDefinition[];
}

/**
 * Task Rules
 */
export type TaskRuleDefinition = RuleDefinition;

/**
 * Group Rules
 */
export type GroupRuleDefinition = RuleAllDefinition | RuleSomeDefinition;
export class RuleAllDefinition {
  all: RuleDefinition;
}
export class RuleSomeDefinition {
  some: RuleDefinition;
}

export class Rule {
  @ApiModelProperty({
    description: `Name of event of task's meta field in form of "$variable" or operator definition`,
    example: '$score'
  })
  @IsNotEmpty()
  value: RuleValue;

  @ApiModelProperty({
    description:
      'Comparator name (you can prepend comparison operators with a letter indicating forced casting to type: n - number, s - string, f - float)',
    example: '>',
    enum: Object.keys(ComparatorsService.comparators)
  })
  @IsIn(Object.keys(ComparatorsService.comparators))
  comparator: string;

  @ApiModelProperty({
    description: 'Right side of compare operation',
    example: '60'
  })
  @IsNotEmpty()
  target: ComparableValue;
}

export class AchievementDto {
  @ApiModelProperty({
    example: 'Early Bird',
    required: true
  })
  @IsOptional({ groups: ['update'] })
  @IsNotEmpty()
  @IsString()
  name: string;

  @ApiModelProperty({
    example: 'Awarded for immediate solving of problems'
  })
  @IsOptional()
  @IsString()
  description: string;

  @ApiModelProperty({
    example: 0,
    type: Number,
    description: '0 - GROUP, 1 - TASK'
  })
  @IsOptional({ groups: ['update'] })
  @IsNotEmpty()
  @IsIn([0, 1])
  scope: number;

  @ApiModelProperty({
    description: `Names of event fields that trigger evaluation`,
    example: ['occurredAt'],
    isArray: true,
    required: true
  })
  @IsOptional({ groups: ['update'] })
  @IsArray()
  @ArrayNotEmpty()
  triggeredBy: string[];

  @IsOptional()
  @IsNumber()
  @ApiModelProperty({
    description: 'version of rules definition API to use, 1 for old limited custom logic, 2 for json-logic syntax',
    type: Number,
    default: 1,
    required: false
  })
  rulesApiVersion?: number = 1;

  @ApiModelProperty({
    description: 'Group ids that have this achivement assigned',
    example: ['123', '456'],
    type: String,
    isArray: true,
    required: true
  })
  @IsOptional({ groups: ['update'] })
  @IsArray({ groups: ['create', 'update'] })
  @ArrayNotEmpty({ groups: ['create', 'update'] })
  availableInGroups: string[];

  @ApiModelProperty({
    description: 'Image representing this achievement',
    example: 'http://cdn.course.com/images/achievement_123.png',
    type: String,
    required: false
  })
  @IsOptional()
  @IsString()
  image: string;

  @ApiModelProperty({
    description: 'Rules definitions for each level of achievement',
    example: [
      {
        value: [
          'timediff',
          {
            start: '$startsAt',
            end: '$occurredAt',
            in: 'days'
          }
        ],
        comparator: '<',
        target: 1
      }
    ],
    isArray: true,
    required: true,
    type: Rule
  })
  @IsOptional({ groups: ['update'] })
  @IsArray()
  @ArrayNotEmpty()
  @ValidateNested()
  rules: GroupRuleDefinition[] | TaskRuleDefinition[] | RulesLogic[];
}
