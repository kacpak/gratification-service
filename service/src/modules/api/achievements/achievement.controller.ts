import { Get, Controller, UseGuards, Post, Body, Param, Put, Delete, ValidationPipe, UsePipes } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiUseTags } from '@nestjs/swagger';
import { updateDefinedObjectKeys } from '../../../utils';
import { AchievementDto } from './achievement.dto';
import { Achievement } from './achievement.entity';
import { AchievementService } from './achievement.service';
import { GroupService } from '../group/group.service';
import { logger } from '../../../logger';

@Controller('project/:projectId/achievement')
@UseGuards(AuthGuard('bearer'))
@ApiUseTags('Project')
@ApiBearerAuth()
@ApiResponse({ status: 401, description: 'Not authorized.' })
export class AchievementController {
  constructor(private readonly achievementService: AchievementService, private readonly groupService: GroupService) {}

  @Get('/')
  @ApiOperation({ title: 'List all achievements for given project' })
  async getProjectsAchievements(@Param('projectId') projectId: string) {
    return await this.achievementService.findAll({
      where: {
        projectId
      }
    });
  }

  @Put(':achievementId')
  @ApiOperation({ title: 'Modify given achievement' })
  @UsePipes(new ValidationPipe({ groups: ['update'] }))
  async updateAchievement(
    @Param('projectId') projectId: string,
    @Param('achievementId') achievementId: string,
    @Body() updatedAchievement: AchievementDto
  ) {
    const achievement = await this.achievementService.find({
      where: {
        projectId,
        id: achievementId
      }
    });

    const updated: Achievement = updateDefinedObjectKeys(achievement, {
      name: updatedAchievement.name,
      description: updatedAchievement.description,
      scope: updatedAchievement.scope,
      rules: updatedAchievement.rules,
      triggeredBy: updatedAchievement.triggeredBy,
      image: updatedAchievement.image,
      rulesApiVersion: updatedAchievement.rulesApiVersion
    });

    const res = await this.achievementService.save(updated);
    logger.log(`Updated achievement: ${JSON.stringify(res)}`);
    return res;
  }

  @Delete(':achievementId')
  @ApiOperation({ title: 'Delete given achievement' })
  async delete(@Param('projectId') projectId: string, @Param('achievementId') achievementId: string) {
    return this.achievementService.delete({
      projectId,
      id: achievementId
    });
  }

  @Post('/')
  @ApiOperation({ title: 'Create new achievement in project' })
  @UsePipes(new ValidationPipe({ groups: ['create'] }))
  async createNewAchievement(@Body() newAchievement: AchievementDto, @Param('projectId') projectId: string) {
    const groups = await Promise.all(newAchievement.availableInGroups.map(id => this.groupService.findById(id)));

    const achievement = this.achievementService.create({
      name: newAchievement.name,
      description: newAchievement.description,
      scope: newAchievement.scope,
      rules: newAchievement.rules,
      rulesApiVersion: newAchievement.rulesApiVersion,
      triggeredBy: newAchievement.triggeredBy,
      image: newAchievement.image,
      availableInGroups: groups,
      projectId
    });
    const res = await this.achievementService.save(achievement);
    logger.log(`Created new achievement: ${JSON.stringify(res)}`);
    return res;
  }
}
