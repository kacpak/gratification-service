import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, OneToMany, ManyToMany } from 'typeorm';
import { Project } from '../project/project.entity';
import { GroupRuleDefinition, ProviderValue, TaskRuleDefinition } from './achievement.dto';
import { AcquiredAchievement } from './acquiredAchievement.entity';
import { Group } from '../group/group.entity';
import { RulesLogic } from 'json-logic-js';

export enum Scope {
  GROUP = 0,
  TASK = 1
}

@Entity()
export class Achievement {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  name: string;

  @Column()
  description: string;

  @Column('int', { default: Scope.TASK })
  scope: Scope;

  @Column('text', { default: '' })
  image: string = '';

  /**
   * Name of `provides` field that will trigger reevaluation
   */
  @Column('jsonb')
  triggeredBy: ProviderValue[];

  @Column('int', { default: 1 })
  rulesApiVersion: number;

  @Column('jsonb')
  rules: GroupRuleDefinition[] | TaskRuleDefinition[] | RulesLogic[];

  @ManyToOne(type => Project, project => project.achievements)
  project: Project;

  @Column()
  projectId: string;

  @ManyToMany(type => Group, group => group.assignedAchievements)
  availableInGroups: Group[];

  @OneToMany(type => AcquiredAchievement, acquiredAchievement => acquiredAchievement.achievement, { onDelete: 'CASCADE' })
  acquired: AcquiredAchievement[];
}
