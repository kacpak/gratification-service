import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Brackets, DeleteResult, FindConditions, FindManyOptions, FindOneOptions, ObjectID, RemoveOptions, Repository } from 'typeorm';
import { ProviderValue } from './achievement.dto';
import { Achievement } from './achievement.entity';
import { AcquiredAchievement } from './acquiredAchievement.entity';
import { GroupService } from '../group/group.service';
import { logger } from "../../../logger";

@Injectable()
export class AchievementService {
  constructor(
    @InjectRepository(Achievement) private readonly achievementRepository: Repository<Achievement>,
    @InjectRepository(AcquiredAchievement) private readonly acquiredAchievementRepository: Repository<AcquiredAchievement>,
    private readonly groupService: GroupService
  ) {}

  create(achievement: Partial<Achievement>): Achievement {
    return this.achievementRepository.create(achievement);
  }

  createAcquiredAchievement(acquiredAchievement: Partial<AcquiredAchievement>): AcquiredAchievement {
    return this.acquiredAchievementRepository.create(acquiredAchievement);
  }

  async findAcquiredForGroup(actorId: string, achievementId: string, forGroupId: string): Promise<AcquiredAchievement> {
    return this.acquiredAchievementRepository.findOne({
      where: {
        actorId,
        achievementId,
        forGroupId
      }
    });
  }

  async findAcquiredForTask(actorId: string, achievementId: string, forTaskId: string): Promise<AcquiredAchievement> {
    return this.acquiredAchievementRepository.findOne({
      where: {
        actorId,
        achievementId,
        forTaskId
      }
    });
  }

  async find(options?: FindOneOptions<Achievement>): Promise<Achievement> {
    return this.achievementRepository.findOne(options);
  }

  async findAll(options?: FindManyOptions<Achievement>): Promise<Achievement[]> {
    return this.achievementRepository.find(options);
  }

  async delete(
    criteria: string | string[] | number | number[] | Date | Date[] | ObjectID | ObjectID[] | FindConditions<Achievement>
  ): Promise<DeleteResult> {
    return this.achievementRepository.delete(criteria);
  }

  async getAchievementsOfGroup(groupId: string): Promise<Achievement[]> {
    const group = await this.groupService.findById(groupId, {
      relations: ['assignedAchievements']
    });
    logger.log('GROUP ' + JSON.stringify(group));

    return group.assignedAchievements;
  }

  async findAllForProjectTriggeredBy(projectId: string, triggeredBy: ProviderValue[]): Promise<Achievement[]> {
    return this.achievementRepository
      .createQueryBuilder('achievement')
      .where('"achievement"."projectId" = :projectId', { projectId })
      .andWhere(
        new Brackets(qb => {
          triggeredBy.forEach((value, i) => {
            if (i === 0) {
              qb.where(`achievement.triggeredBy @> :${i}`, {
                [i]: JSON.stringify(value)
              });
            } else {
              qb.orWhere(`achievement.triggeredBy @> :${i}`, {
                [i]: JSON.stringify(value)
              });
            }
          });
        })
      )
      .getMany();
  }

  async save(...achievements: Achievement[]): Promise<Achievement[]> {
    return await this.achievementRepository.save(achievements);
  }

  async saveAcquiredAchievement(
    acquiredAchievements: AcquiredAchievement | AcquiredAchievement[]
  ): Promise<AcquiredAchievement | AcquiredAchievement[]> {
    if (Array.isArray(acquiredAchievements)) {
      return await this.acquiredAchievementRepository.save(acquiredAchievements);
    }
    return await this.acquiredAchievementRepository.save(acquiredAchievements);
  }
}
