import { Entity, Column, Index, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { Actor } from '../actor/actor.entity';
import { Achievement } from './achievement.entity';
import { Task } from '../task/task.entity';
import { Group } from '../group/group.entity';

@Entity()
@Index(['actorId', 'achievementId', 'forTaskId'], { unique: true })
@Index(['actorId', 'achievementId', 'forGroupId'], { unique: true })
export class AcquiredAchievement {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(type => Actor, actor => actor.acquiredAchievements)
  actor: Actor;

  @Column({ nullable: false })
  actorId: string;

  @ManyToOne(type => Achievement, achievement => achievement.acquired, {
    eager: true
  })
  achievement: Achievement;

  @Column({ nullable: false })
  achievementId: string;

  @Column()
  level: number;

  @Column({ nullable: true })
  forGroupId: string;

  @ManyToOne(type => Group, {eager: true})
  forGroup: Group;

  @Column({ nullable: true })
  forTaskId: string;

  @ManyToOne(type => Task, {eager: true})
  forTask: Task;
}
