import { Entity, Column, ManyToOne, PrimaryGeneratedColumn, Index } from 'typeorm';
import { Task } from '../task/task.entity';
import { Actor } from '../actor/actor.entity';

@Entity()
export class Event {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({
    default: 'activity'
  })
  @Index()
  type: 'init' | 'activity';

  @Column({ nullable: false })
  actorId: string;

  @ManyToOne(type => Actor, actor => actor.events)
  actor: Actor;

  @Column({ nullable: false })
  @Index()
  taskId: string;

  @ManyToOne(type => Task, task => task.events)
  task: Task;

  @Column('jsonb')
  data: any;

  @Column('timestamp')
  occurredAt: Date;
}
