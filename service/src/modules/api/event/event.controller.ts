import { Controller, Post, Body, Get, Param, UseGuards, Req, HttpException, HttpStatus } from '@nestjs/common';
import { ProjectAccessGuard } from '../project/project.guard';
import { EventDto } from './event.dto';
import { EventQueue } from './event.queue';
import { ApiUseTags, ApiBearerAuth, ApiResponse } from '@nestjs/swagger';
import { EventService } from './event.service';
import { AuthGuard } from '@nestjs/passport';
import { logger } from "../../../logger";

@Controller('event')
@ApiUseTags('Event')
@UseGuards(ProjectAccessGuard)
@UseGuards(AuthGuard('bearer'))
@ApiBearerAuth()
@ApiResponse({ status: 401, description: 'Not authorized.' })
export class EventController {
  constructor(private readonly eventQueue: EventQueue, private readonly eventService: EventService) {}

  @Get(':projectId')
  async list(@Param('projectId') projectId: string, @Req() req) {
    const events = await this.eventService.findAll(projectId);
    logger.log(`User "${req.user.login}" requested all events`, 'EventController');
    return events;
  }

  @Post(':projectId')
  async push(@Body() event: EventDto, @Param('projectId') projectId: string, @Req() req) {
    if (!(await this.eventService.validateEventDependencies(event.taskId, projectId))) {
      throw new HttpException(`Event "${JSON.stringify(event)}" targets not existing project or task`, HttpStatus.BAD_REQUEST);
    }
    logger.log(`User "${req.user.login}" pushed new event`, 'EventController');
    this.eventQueue.push(event, projectId);
  }
}
