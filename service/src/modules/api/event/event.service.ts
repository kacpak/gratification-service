import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Group } from '../group/group.entity';
import { ProjectService } from '../project/project.service';
import { Task } from '../task/task.entity';
import { TaskService } from '../task/task.service';
import { Event } from './event.entity';
import { Actor } from '../actor/actor.entity';

@Injectable()
export class EventService {
  constructor(
    @InjectRepository(Event) private readonly eventRepository: Repository<Event>,
    @InjectRepository(Task) private readonly taskRepository: Repository<Task>,
    private readonly projectService: ProjectService,
    private readonly taskService: TaskService
  ) {}

  getRepository(): Repository<Event> {
    return this.eventRepository;
  }

  create(event: Partial<Event>): Event {
    return this.eventRepository.create(event);
  }

  async validateEventDependencies(localTaskId: string, projectId: string): Promise<boolean> {
    try {
      const project = await this.projectService.findById(projectId);
      const task = await this.taskService.findByLocalTaskId(projectId, localTaskId);
      return !!project && !!task;
    } catch (e) {
      return false;
    }
  }

  async findAllOfGroup(groupId: string, actorId: number): Promise<Event[]> {
    return this.eventRepository
      .createQueryBuilder('event')
      .leftJoinAndSelect('event.task', 'task', 'event.taskId = task.id')
      .innerJoin(
        qb =>
          qb
            .from(Event, 'e2')
            .select('"e2"."taskId"', 'taskId')
            .addSelect('"e2"."actorId"', 'actorId')
            .addSelect('MAX("e2"."occurredAt")', 'occurredAt')
            .groupBy('"e2"."taskId"')
            .addGroupBy('"e2"."actorId"'),
        'inner',
        '"event"."taskId" = "inner"."taskId" AND "event"."occurredAt" = "inner"."occurredAt" AND "event"."actorId" = "inner"."actorId"'
      )
      .where('"task"."groupId" = :groupId', { groupId })
      .andWhere('"event"."actorId" = :actorId', { actorId })
      .getMany();
  }

  async findAll(projectId: string): Promise<Event[]> {
    return this.eventRepository
      .createQueryBuilder('event')
      .leftJoin(Task, 'task', 'event.taskId = task.id')
      .leftJoin(Group, 'group', 'task.groupId = group.id')
      .where('"group"."projectId" = :projectId', { projectId })
      .orderBy('event.occurredAt', 'DESC')
      .getMany();
  }

  async save(events: Event | Event[]): Promise<Event | Event[]> {
    if (Array.isArray(events)) {
      return await this.eventRepository.save(events);
    }
    return await this.eventRepository.save(events);
  }
}
