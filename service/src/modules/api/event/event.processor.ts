import { Injectable } from '@nestjs/common';
import { ComparatorsService } from '../../evaluation/comparators/comparators.service';
import { OperatorsService } from '../../evaluation/operators/operators.service';
import { getValue } from '../../evaluation/utils';
import {
  ComparableValue,
  GroupRuleDefinition,
  Rule,
  RuleAllDefinition,
  RuleAndDefinition,
  RuleDefinition,
  RuleOrDefinition,
  RuleSomeDefinition,
  RuleValue,
  TaskRuleDefinition
} from '../achievements/achievement.dto';
import { Scope } from '../achievements/achievement.entity';
import { AchievementService } from '../achievements/achievement.service';
import { AcquiredAchievement } from '../achievements/acquiredAchievement.entity';
import { Actor } from '../actor/actor.entity';
import { Project } from '../project/project.entity';
import { Task } from '../task/task.entity';
import { TaskService } from '../task/task.service';
import { CallbackQueue } from './callback.queue';
import { Event } from './event.entity';
import { EventService } from './event.service';
import { logger } from '../../../logger';
import { apply as applyJSONLogic, RulesLogic } from 'json-logic-js';

@Injectable()
export class EventProcessor {
  constructor(
    private readonly achievementService: AchievementService,
    private readonly comparatorsService: ComparatorsService,
    private readonly callbackQueue: CallbackQueue,
    private readonly eventService: EventService,
    private readonly operatorsService: OperatorsService,
    private readonly taskService: TaskService
  ) {}

  async process(event: Event, actor: Actor, project: Project) {
    const log = message => logger.log(`[Event "${event.id}"][Actor "${actor.localId}"] ${message}`, 'EventProcessor');
    if (!event.actor) {
      event.actor = actor;
    }

    log(`Processing ${JSON.stringify(event)}`);

    let gotAchievement = false;
    const achievements = await this.achievementService.getAchievementsOfGroup(event.task.groupId);
    log(`Found ${achievements.length} matching achievements to re-evaluate`);

    try {
      let tasksWithLatestEvents: Task[];

      if (achievements.some(achievement => achievement.scope === Scope.GROUP)) {
        log(`Querying all tasks in group with latest associated events`);
        tasksWithLatestEvents = await this.taskService.findTasksWithLatestEvents(event.task.groupId, actor.id);
      }

      for (const achievement of achievements) {
        let acquiredAchievement: AcquiredAchievement;

        log(`Querying acquired achievements to test if calculations are necessary`);
        if (achievement.scope === Scope.GROUP) {
          acquiredAchievement = await this.achievementService.findAcquiredForGroup(event.actorId, achievement.id, event.task.groupId);
        } else if (achievement.scope === Scope.TASK) {
          acquiredAchievement = await this.achievementService.findAcquiredForTask(event.actorId, achievement.id, event.taskId);
        }

        // If achievement already acquired stop further evaluation
        if (acquiredAchievement && acquiredAchievement.level >= achievement.rules.length) {
          log(`Achievement "${achievement.name}" skipped due to being already acquired`);
          continue;
        }

        let lastApplicableLevel: number = -1;
        log(`Calculating each achievement rule to test if it's applicable to actor, based on definitions`);
        for (let i: number = 0; i < achievement.rules.length; i++) {
          const definition = achievement.rules[i];

          let isApplicable: boolean = false;
          log(`Calculating "${i}" rule`);

          try {
            if (achievement.rulesApiVersion === 2) {
              isApplicable = await this.calculateJSONLogicRule(project, event, tasksWithLatestEvents, definition as RulesLogic, actor);
            } else if (achievement.rulesApiVersion === 1) {
              if (achievement.scope === Scope.TASK) {
                isApplicable = await this.calculateTaskScopedRule(event, definition as TaskRuleDefinition);
              } else if (achievement.scope === Scope.GROUP) {
                isApplicable = await this.calculateGroupScopedRule(tasksWithLatestEvents, definition as GroupRuleDefinition, actor);
              }
            }
          } catch (e) {
            logger.error(
              `[Event "${event.id}"][Actor "${event.actorId}"] Error in calculating rules with v${achievement.rulesApiVersion} API. ${e.message}`,
              e.stack,
              'EventProcessor'
            );
          }
          log(`Rule "${i}" is ${isApplicable ? 'applicable' : 'NOT applicable'} to actor`);

          if (isApplicable) {
            lastApplicableLevel = i;
          }
        }

        // If not applicable stop further evaluation
        if (lastApplicableLevel < 0) {
          log(`Achievement "${achievement.name}" not applicable to actor`);
          continue;
        } else if (acquiredAchievement && acquiredAchievement.level === lastApplicableLevel) {
          log(`Achievement "${achievement.name}" level "${acquiredAchievement.level}" already achieved earlier`);
          continue;
        }

        try {
          if (!acquiredAchievement || acquiredAchievement.level !== lastApplicableLevel) {
            log(`Achievement "${achievement.name}" level "${lastApplicableLevel}" acquired`);

            if (!acquiredAchievement) {
              acquiredAchievement = this.achievementService.createAcquiredAchievement({
                actor,
                achievementId: achievement.id,
                level: lastApplicableLevel
              });
            } else {
              acquiredAchievement.level = lastApplicableLevel;
            }

            if (achievement.scope === Scope.GROUP) {
              acquiredAchievement.forGroup = event.task.group;
            } else if (achievement.scope === Scope.TASK) {
              acquiredAchievement.forTask = event.task;
            }

            await this.achievementService.saveAcquiredAchievement(acquiredAchievement);
            gotAchievement = true;
          }
        } catch (e) {
          logger.error(`[Event "${event.id}"][Actor "${event.actorId}"] ${e.message}`, e.stack, 'EventProcessor');
        }
      }
    } catch (e) {
      logger.error(`[Event "${event.id}"][Actor "${event.actorId}"] ${e.message}`, e.stack, 'EventProcessor');
    }
    log(`Finished evaluating achievements`);

    if (gotAchievement) {
      const callback = {
        status: 'NEW_ACHIEVEMENT',
        projectId: project.id,
        userId: actor.localId
      };
      log(`Sending callback to "${project.callbackUrl}": ${JSON.stringify(callback)}`);
      this.sendCallbackToClient(project.callbackUrl, callback);
    }
  }

  /**
   *
   * @param event
   * @param tasksInGroup available only with Scope.GROUP
   * @param rule
   * @param actor
   */
  async calculateJSONLogicRule(
    project: Project,
    event: Event,
    tasksInGroup: Task[] | undefined,
    rule: RulesLogic,
    actor: Actor
  ): Promise<boolean> {
    // const collectRulesVariables = (): string[] => {
    //   const traverseObject = (obj: RulesLogic, callback: (key: string, value: RulesLogic) => void): void => {
    //     Object.keys(obj).forEach(key => {
    //       if (obj[key] && typeof obj[key] === 'object') {
    //         traverseObject(obj[key], callback);
    //       } else {
    //         callback(key, obj[key]);
    //       }
    //     });
    //   };
    //
    //   const variablesSet = new Set<string>();
    //   traverseObject(rule, (key, value) => {
    //     if (key === 'var' && typeof value === 'string') {
    //       variablesSet.add(value);
    //     }
    //   });
    //   return [...variablesSet];
    // };
    //
    // const variables = collectRulesVariables();
    const data = {
      event: {
        data: event.data,
        id: event.id,
        occurredAt: event.occurredAt,
        type: event.type
      },
      actor: {
        id: event.actor.id,
        localId: event.actor.localId
      },
      task: {
        meta: event.task.meta,
        id: event.task.id,
        localId: event.task.localId,
        name: event.task.name,
        provides: event.task.provides
      },
      group: {
        id: event.task.group.id,
        meta: event.task.group.meta,
        name: event.task.group.name,
        description: event.task.group.description
      },
      project: {
        id: project.id,
        name: project.name,
        isABTested: project.isABTested
      },
      tasksInGroup: tasksInGroup
        ? tasksInGroup.map(task => {
            const lastEvent = task.events && task.events.length > 0 ? task.events[0] : undefined;

            return {
              id: task.id,
              meta: task.meta,
              name: task.name,
              provides: task.provides,
              lastEvent: lastEvent
                ? {
                    id: lastEvent.id,
                    data: lastEvent.data,
                    occurredAt: lastEvent.occurredAt,
                    type: lastEvent.type
                  }
                : undefined
            };
          })
        : undefined
    } as const;

    return applyJSONLogic(rule, data);
  }

  async calculateGroupScopedRule(tasks: Task[], rule: GroupRuleDefinition, actor: Actor) {
    if (!('all' in rule || 'some' in rule)) {
      throw new Error(`Group scoped achievement needs to enclose its rules in 'all' or 'some' definition property`);
    }

    if ('all' in rule && !tasks.every(task => task.events && task.events.length > 0)) {
      return false;
    }

    const events = tasks
      .filter(task => task.events && task.events.length > 0)
      .map(task => {
        const event = task.events[0];
        event.task = task;
        event.actor = actor;
        return event;
      });

    return this.processEventsForGroup(events, rule);
  }

  async processEventsForGroup(events: Event[], rule: GroupRuleDefinition): Promise<boolean> {
    if (!('all' in rule || 'some' in rule)) {
      throw new Error(`Group scoped achievement needs to enclose its rules in 'all' or 'some' definition property`);
    }

    const definition: RuleDefinition =
      'all' in rule ? (rule as RuleAllDefinition).all : 'some' in rule ? (rule as RuleSomeDefinition).some : undefined;

    const checks = await Promise.all(events.map(event => this.calculateTaskScopedRule(event, definition)));

    if ('all' in rule) {
      return checks.every(check => check);
    } else if ('some' in rule) {
      return checks.some(check => check);
    }

    return false;
  }

  async calculateTaskScopedRule(event: Event, rule: TaskRuleDefinition): Promise<boolean> {
    const log = (msg: string) => logger.log(`[Event "${event.id}"][Actor "${event.actorId}"] ${msg}`, 'EventProcessor');
    if ('all' in rule || 'some' in rule) {
      throw new Error(`Task scoped achievement cannot be enclosed in 'all' or 'some' definition property`);
    }

    const type = 'and' in rule ? 'and' : 'or' in rule ? 'or' : 'plain';

    if (type !== 'plain') {
      let ruleDefinitions: RuleDefinition[];

      if ('and' in rule) {
        ruleDefinitions = (rule as RuleAndDefinition).and;
      } else if ('or' in rule) {
        ruleDefinitions = (rule as RuleOrDefinition).or;
      }

      const rules = await Promise.all(ruleDefinitions.map(andRule => this.calculateTaskScopedRule(event, andRule)));

      if (type === 'and') {
        const res = rules.every(result => result);
        log(`Calculating "${type}": ${JSON.stringify(res)}`);
        return res;
      } else if (type === 'or') {
        const res = rules.some(result => result);
        log(`Calculating "${type}": ${JSON.stringify(res)}`);
        return res;
      }
    } else {
      return this.calculateRule(event, rule as Rule);
    }
  }

  async calculateRule(event: Event, rule: Rule): Promise<boolean> {
    const log = (msg: string) => logger.log(`[Event "${event.id}"][Actor "${event.actorId}"] ${msg}`, 'EventProcessor');
    log(`Calculating rule: "${JSON.stringify(rule)}"`);
    const value = await this.calculateValue(event, rule.value);
    const comparator = this.comparatorsService.getComparator(rule.comparator);
    const target = getValue(event, rule.target);
    log(`Calculating resolved rule: ${JSON.stringify(value)} ${rule.comparator} ${JSON.stringify(target)}`);
    const result = await comparator(value, target);
    log(`Calculation result: ${JSON.stringify(result)}`);
    return result;
  }

  async calculateValue(event: Event, value: RuleValue): Promise<ComparableValue> {
    if (typeof value === 'string') {
      return getValue(event, value);
    } else {
      const operator = this.operatorsService.getOperator(value[0]);
      if (!operator) {
        return;
      }
      return await operator({
        event,
        options: value[1]
      });
    }
  }

  sendCallbackToClient(url: string, data: any) {
    if (url) {
      this.callbackQueue.push({ url, data });
    }
  }
}
