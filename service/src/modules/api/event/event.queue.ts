import { ProjectService } from '../project/project.service';
import { Event } from './event.entity';
import { EventDto } from './event.dto';
import { Injectable } from '@nestjs/common';
import { EventProcessor } from './event.processor';
import { EventService } from './event.service';
import { Project } from '../project/project.entity';
import { Actor } from '../actor/actor.entity';
import { ActorService } from '../actor/actor.service';
import { TaskService } from '../task/task.service';
import { logger } from '../../../logger';

interface QueuedEvent extends EventDto {
  projectId: string;
}

@Injectable()
export class EventQueue {
  private queue: QueuedEvent[] = [];
  private isRunning: boolean = false;

  constructor(
    private readonly eventService: EventService,
    private readonly eventProcessor: EventProcessor,
    private readonly actorService: ActorService,
    private readonly taskService: TaskService,
    private readonly projectService: ProjectService
  ) {}

  push(event: EventDto, projectId: string) {
    this.queue.unshift({
      ...event,
      projectId
    });

    if (!this.isRunning) {
      this.next();
    }
  }

  private async next() {
    if (this.queue.length === 0) {
      this.isRunning = false;
      return;
    }

    this.isRunning = true;
    await this.process(this.queue.pop());
    this.next();
  }

  async getActorForEvent(localActorId: string, project: Project): Promise<Actor> {
    const actor = await this.actorService.find(localActorId, project.id);

    if (actor) {
      return actor;
    }

    logger.log(`Actor by local id "${localActorId}" not found. Creating actor.`, 'EventQueue');
    const newActor = await this.actorService.create({
      localId: localActorId,
      hasAchievements: project.isABTested ? Math.random() >= 0.5 : true,
      project
    });

    return this.actorService.save(newActor) as Promise<Actor>;
  }

  private async process(event: QueuedEvent) {
    try {
      logger.log(`Started processing event "${JSON.stringify(event)}"`, 'EventQueue');

      const [task, project] = await Promise.all([
        this.taskService.findByLocalTaskId(event.projectId, event.taskId),
        this.projectService.findById(event.projectId)
      ]);
      const actor = await this.getActorForEvent(event.actorId, project);

      const eventEntity: Event = this.eventService.create({
        data: event.data,
        occurredAt: event.occurredAt,
        task,
        actor
      });
      const savedEntity: Event = (await this.eventService.save(eventEntity)) as Event;

      if (actor.hasAchievements) {
        await this.eventProcessor.process(savedEntity, actor, project);
      } else {
        logger.log(`Actor "${actor.localId}" has disabled achievements. They will not be calculated.`, 'EventQueue');
      }
    } catch (e) {
      logger.error(e.message, e.stack, 'EventQueue');
    }
  }
}
