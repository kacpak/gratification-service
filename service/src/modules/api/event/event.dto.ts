import { IsDateString, IsNotEmpty, IsString } from 'class-validator';

export class EventDto {
  @IsNotEmpty()
  @IsString()
  taskId: string;

  @IsString()
  @IsNotEmpty()
  actorId: string;

  @IsNotEmpty()
  data: any;

  @IsDateString()
  occurredAt: Date;
}
