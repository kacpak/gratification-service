import { Injectable } from '@nestjs/common';
import axios from 'axios';
import { logger } from "../../../logger";

export interface Callback {
  url: string;
  data: any;
}

@Injectable()
export class CallbackQueue {
  private queue: Callback[] = [];
  private isRunning: boolean = false;

  constructor() {}

  push(callback: Callback) {
    this.queue.unshift({
      ...callback
    });

    if (!this.isRunning) {
      this.next();
    }
  }

  private async next() {
    if (this.queue.length === 0) {
      this.isRunning = false;
      return;
    }

    this.isRunning = true;
    await this.__process(this.queue.pop());
    this.next();
  }

  async __process(callback: Callback) {
    try {
      await axios.post(callback.url, callback.data, {
        timeout: 10000
      });
      logger.log(`Sent callback to "${callback.url}": ${JSON.stringify(callback.data)}`, 'CallbackQueue');
    } catch (e) {
      logger.error(e.message, e.stack, 'CallbackQueue');
    }
  }
}
