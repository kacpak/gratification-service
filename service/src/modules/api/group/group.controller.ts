import {
  Get,
  Controller,
  UseGuards,
  Post,
  Body,
  Param,
  UsePipes,
  ValidationPipe,
  Put,
  NotFoundException,
  Delete
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiUseTags } from '@nestjs/swagger';
import { updateDefinedObjectKeys } from '../../../utils';
import { ProjectAccessGuard } from '../project/project.guard';
import { GroupService } from './group.service';
import { Group } from './group.entity';
import { NewGroupDto } from './group.dto';
import { ProjectService } from '../project/project.service';
import { logger } from "../../../logger";

@Controller('project/:projectId/group')
@UseGuards(ProjectAccessGuard)
@UseGuards(AuthGuard('bearer'))
@ApiUseTags('Project')
@ApiBearerAuth()
@ApiResponse({ status: 401, description: 'Not authorized.' })
export class GroupController {
  constructor(private readonly groupService: GroupService, private readonly projectService: ProjectService) {}

  @Get('/')
  @ApiOperation({ title: 'List all groups of given project' })
  async getProjectsGroups(@Param('projectId') projectId: string): Promise<Group[]> {
    return await this.groupService.findAll({
      where: {
        projectId
      }
    });
  }

  @Post('/')
  @ApiOperation({ title: 'Create new group in project' })
  @UsePipes(new ValidationPipe({ groups: ['create'] }))
  async createNewGroup(@Body() newGroup: NewGroupDto, @Param('projectId') projectId: string) {
    const project = await this.projectService.findById(projectId);
    const group = await this.groupService.create({
      name: newGroup.name,
      description: newGroup.description,
      meta: newGroup.meta,
      project
    });
    const res = await this.groupService.save(group);
    logger.log(`Created new group: ${JSON.stringify(res)}`, 'GroupController');
    return res;
  }

  @Put('/:groupId')
  @ApiOperation({ title: 'Modify group information in project' })
  @UsePipes(new ValidationPipe({ groups: ['update'], whitelist: true }))
  async updateGroup(@Body() update: NewGroupDto, @Param('projectId') projectId: string, @Param('groupId') id: string) {
    const group = await this.groupService.findById(id, {
      where: {
        projectId
      }
    });
    if (!group) {
      throw new NotFoundException('Requested group does not exist');
    }
    const updated = updateDefinedObjectKeys(group, {
      name: update.name,
      description: update.description,
      meta: update.meta
    });
    const res = await this.groupService.save(updated);
    logger.log(`Updated group: ${JSON.stringify(res)}`, 'GroupController');
    return res;
  }

  @Delete('/:groupId')
  @ApiOperation({ title: 'Delete group in project' })
  async deleteGroup(@Param('projectId') projectId: string, @Param('groupId') id: string) {
    const res = await this.groupService.delete({
      projectId,
      id
    });
    logger.log(`Deleted group "${id}" from project "${projectId}"`, 'GroupController');
    return res;
  }
}
