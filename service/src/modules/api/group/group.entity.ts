import { Entity, Column, ManyToOne, JoinTable, PrimaryGeneratedColumn, OneToMany, ManyToMany } from 'typeorm';
import { Project } from '../project/project.entity';
import { Task } from '../task/task.entity';
import { Achievement } from '../achievements/achievement.entity';

@Entity()
export class Group {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ length: 128 })
  name: string;

  @Column({
    nullable: true
  })
  description: string;

  @ManyToOne(type => Project, project => project.groups)
  project: Project;

  @Column({ nullable: false })
  projectId: string;

  @Column('jsonb', { nullable: true })
  meta: Record<string, any>;

  @OneToMany(type => Task, task => task.group, { onDelete: 'CASCADE' })
  @JoinTable()
  tasks: Task[];

  @ManyToMany(type => Achievement, achievement => achievement.availableInGroups)
  @JoinTable()
  assignedAchievements: Achievement[];
}
