import { IsNotEmpty, IsOptional, IsString, MinLength } from 'class-validator';
import { Group } from './group.entity';
import { ApiModelProperty } from '@nestjs/swagger';

export class NewGroupDto {
  @ApiModelProperty({ description: 'Name of group', example: 'Group name' })
  @IsOptional({ groups: ['update'] })
  @IsNotEmpty()
  @IsString()
  @MinLength(2)
  name: string;

  @ApiModelProperty({ description: 'Description of a group', example: 'This is an example group' })
  @IsOptional()
  @IsString()
  description: string;

  @ApiModelProperty({ description: 'Object of keys and values to use by developer', example: 'Link to internal service that displays something for group' })
  @IsOptional()
  meta?: Record<string, any>;
}

export interface GroupDto extends Group {}
