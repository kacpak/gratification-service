import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, FindOneOptions, FindManyOptions, ObjectID, FindConditions, DeleteResult } from 'typeorm';
import { Group } from './group.entity';

@Injectable()
export class GroupService {
  constructor(@InjectRepository(Group) private readonly groupRepository: Repository<Group>) {}

  create(group: Partial<Group>): Group {
    return this.groupRepository.create(group);
  }

  findById(id: string, options?: FindOneOptions<Group>): Promise<Group> {
    return this.groupRepository.findOne(id, options);
  }

  findAll(options?: FindManyOptions<Group>): Promise<Group[]> {
    return this.groupRepository.find(options);
  }

  async save(...groups: Group[]): Promise<Group[]> {
    return await this.groupRepository.save(groups);
  }

  async delete(
    criteria: string | string[] | number | number[] | Date | Date[] | ObjectID | ObjectID[] | FindConditions<Group>
  ): Promise<DeleteResult> {
    return await this.groupRepository.delete(criteria);
  }
}
