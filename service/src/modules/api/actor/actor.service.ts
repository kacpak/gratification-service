import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, FindConditions, ObjectID, Repository } from 'typeorm';
import { Actor } from './actor.entity';

@Injectable()
export class ActorService {
  constructor(@InjectRepository(Actor) private readonly actorRepository: Repository<Actor>) {}

  create(actor: Partial<Actor>): Actor {
    return this.actorRepository.create(actor);
  }

  async find(localActorId: string, projectId: string): Promise<Actor> {
    return this.actorRepository.findOne({
      where: {
        localId: localActorId,
        projectId
      }
    });
  }

  async findAll(projectId: string): Promise<Actor[]> {
    return this.actorRepository.find({
      where: {
        projectId
      }
    });
  }

  async save(actors: Actor | Actor[]): Promise<Actor | Actor[]> {
    if (Array.isArray(actors)) {
      return await this.actorRepository.save(actors);
    }
    return await this.actorRepository.save(actors);
  }

  async delete(
    criteria: string | string[] | number | number[] | Date | Date[] | ObjectID | ObjectID[] | FindConditions<Actor>
  ): Promise<DeleteResult> {
    return await this.actorRepository.delete(criteria);
  }
}
