import { Controller, Get, Param, UseGuards, Req, Put, Body, Delete } from '@nestjs/common';
import { ApiUseTags, ApiBearerAuth, ApiResponse, ApiOperation, ApiModelProperty } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { updateDefinedObjectKeys } from '../../../utils';
import { ProjectAccessGuard } from '../project/project.guard';
import { ActorService } from './actor.service';
import { logger } from "../../../logger";

class ActorDTO {
  @ApiModelProperty({
    description: `Actor's ID in external system`,
    example: 'EXTERNAL-12345'
  })
  localId: string;

  @ApiModelProperty({
    description: `Whether actor should be able to earn achievements`,
    type: Boolean
  })
  hasAchievements: boolean;
}

@Controller('/project/:projectId/actor')
@ApiUseTags('Event')
@UseGuards(ProjectAccessGuard)
@UseGuards(AuthGuard('bearer'))
@ApiBearerAuth()
@ApiResponse({ status: 401, description: 'Not authorized.' })
export class ActorController {
  constructor(private readonly actorService: ActorService) {}

  @Get('/')
  @ApiOperation({ title: 'Get all actors data for given project' })
  async list(@Param('projectId') projectId: string, @Req() req) {
    logger.log(`User "${req.user.login}" requested all actors`, 'EventController');
    return await this.actorService.findAll(projectId);
  }

  @Get('/:localActorId')
  @ApiOperation({ title: 'Get given actor data for project' })
  async getActor(@Param('projectId') projectId: string, @Param('localActorId') localActorId: string, @Req() req) {
    logger.log(`User "${req.user.login}" requested actor "${localActorId}"`, 'EventController');
    return await this.actorService.find(localActorId, projectId);
  }

  @Put('/:localActorId')
  @ApiOperation({ title: `Modify actor in project` })
  async modifyActor(@Body() dto: ActorDTO, @Param('projectId') projectId: string, @Param('localActorId') localActorId: string) {
    const actor = await this.actorService.find(localActorId, projectId);
    const updated = updateDefinedObjectKeys(actor, {
      hasAchievements: dto.hasAchievements,
      localId: dto.localId
    });
    const res = await this.actorService.save(updated);
    logger.log(`Actor "${JSON.stringify(res)}" modified`, 'EventController');
    return res;
  }

  @Delete('/:localActorId')
  @ApiOperation({ title: `Delete actor from project` })
  async deleteActor(@Param('projectId') projectId: string, @Param('localActorId') localId: string) {
    const res = await this.actorService.delete({
      projectId,
      localId
    });
    logger.log(`Actor "${localId}" in project "${projectId}" deleted`, 'EventController');
    return res;
  }
}
