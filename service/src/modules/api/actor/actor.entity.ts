import { Entity, Column, OneToMany, Index, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { AcquiredAchievement } from '../achievements/acquiredAchievement.entity';
import { Event } from '../event/event.entity';
import { Project } from '../project/project.entity';

@Entity()
@Index(['localId', 'projectId'], { unique: true })
export class Actor {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  localId: string;

  @ManyToOne(type => Project, project => project.actors)
  project: Project;

  @Column({ nullable: false })
  projectId: string;

  @Column({
    default: true,
    comment: 'Indicates whether actor will be able to acquire achievements or not (this is used for A/B testing functionality)'
  })
  hasAchievements: boolean;

  @OneToMany(type => Event, event => event.actor)
  events: Event[];

  @OneToMany(type => AcquiredAchievement, acquired => acquired.actor, { eager: true })
  acquiredAchievements: AcquiredAchievement[];
}
