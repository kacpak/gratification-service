import { ApiModelProperty } from '@nestjs/swagger';
import { IsArray, IsOptional, IsString } from 'class-validator';
import { Task } from './task.entity';

export class NewTaskDto {
  @ApiModelProperty()
  @IsOptional({ groups: ['update'] })
  @IsString()
  name: string;

  @ApiModelProperty()
  @IsOptional({ groups: ['update'] })
  @IsString()
  taskId: string;

  @ApiModelProperty()
  @IsOptional({ groups: ['update'] })
  meta: any;

  @ApiModelProperty()
  @IsOptional({ groups: ['update'] })
  @IsArray()
  provides: string[];
}

export interface TaskDto extends Task {}
