import {
  Get,
  Controller,
  UseGuards,
  Post,
  Body,
  Param,
  UsePipes,
  ValidationPipe,
  Put,
  NotFoundException,
  Delete
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiUseTags } from '@nestjs/swagger';
import { updateDefinedObjectKeys } from '../../../utils';
import { ProjectAccessGuard } from '../project/project.guard';
import { TaskService } from './task.service';
import { Task } from './task.entity';
import { NewTaskDto } from './task.dto';
import { logger } from "../../../logger";

@Controller('project/:projectId/group/:groupId/task')
@UseGuards(ProjectAccessGuard)
@UseGuards(AuthGuard('bearer'))
@ApiUseTags('Project')
@ApiBearerAuth()
@ApiResponse({ status: 401, description: 'Not authorized.' })
export class TaskController {
  constructor(private readonly taskService: TaskService) {}

  @Get('/')
  @ApiOperation({ title: 'List all tasks of given group' })
  async getGroupsTasks(@Param('groupId') groupId: string): Promise<Task[]> {
    return await this.taskService.findAll({
      where: {
        groupId
      }
    });
  }

  @Get('/:taskId')
  @ApiOperation({ title: 'Get given task' })
  async getTask(@Param('taskId') taskId: string): Promise<Task> {
    return await this.taskService.findById(taskId);
  }

  @Post('/')
  @ApiOperation({ title: 'Create new task in a group' })
  @UsePipes(new ValidationPipe({ groups: ['create'] }))
  async createNewTask(@Body() newTaskDto: NewTaskDto, @Param('groupId') groupId: string) {
    const { name, meta, provides, taskId } = newTaskDto;
    const task = await this.taskService.create({
      localId: taskId,
      name,
      meta,
      provides,
      groupId
    });
    const res = await this.taskService.save(task);
    logger.log(`Created new task: ${JSON.stringify(res)}`, 'TaskController');
    return res;
  }

  @Put('/:taskId')
  @ApiOperation({ title: 'Modify task' })
  @UsePipes(new ValidationPipe({ groups: ['create'] }))
  async modifyTask(@Body() dto: NewTaskDto, @Param('groupId') groupId: string, @Param('taskId') id: string) {
    const task = await this.taskService.findById(id, {
      where: {
        groupId
      }
    });
    if (!task) {
      throw new NotFoundException(`Task not found`);
    }
    const updated = updateDefinedObjectKeys(task, {
      name: dto.name,
      meta: dto.meta,
      provides: dto.provides,
      localId: dto.taskId
    });
    const res = await this.taskService.save(updated);
    logger.log(`Modified task: ${JSON.stringify(res)}`, 'TaskController');
    return res;
  }

  @Delete('/:taskId')
  @ApiOperation({ title: 'Delete task from group' })
  async deleteTask(@Param('projectId') projectId: string, @Param('groupId') groupId: string, @Param('taskId') id: string) {
    const res = await this.taskService.delete({
      groupId,
      id
    });
    logger.log(`Deleted group "${id}" from group "${groupId}" from project "${projectId}"`, 'TaskController');
    return res;
  }
}
