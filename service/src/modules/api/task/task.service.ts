import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, FindManyOptions, FindOneOptions, ObjectID, FindConditions, DeleteResult } from 'typeorm';
import { Event } from '../event/event.entity';
import { Project } from '../project/project.entity';
import { Task } from './task.entity';

@Injectable()
export class TaskService {
  constructor(
    @InjectRepository(Task) private readonly taskRepository: Repository<Task>,
    @InjectRepository(Event) private readonly eventRepository: Repository<Event>
  ) {}

  create(task: Partial<Task>): Task {
    return this.taskRepository.create(task);
  }

  findAll(options?: FindManyOptions<Task>): Promise<Task[]> {
    return this.taskRepository.find(options);
  }

  findById(id: string, options?: FindOneOptions): Promise<Task> {
    return this.taskRepository.findOne(id, options);
  }

  async findTasksWithLatestEvents(groupId: string, actorId: number) {
    const subQuery = this.eventRepository
      .createQueryBuilder('e2')
      .select(`MAX("e2"."occurredAt")`)
      .where(`"e"."taskId" = "e2"."taskId" AND "e2"."actorId" = :actorId`);

    return this.taskRepository
      .createQueryBuilder('t')
      .leftJoinAndSelect('t.events', 'e', `"e"."occurredAt" = (${subQuery.getQuery()})`, { actorId })
      .where(`"t"."groupId" = :groupId`, { groupId })
      .getMany();
  }

  async findByLocalTaskId(projectId: string, localTaskId: string): Promise<Task> {
    return this.taskRepository
      .createQueryBuilder('task')
      .leftJoinAndSelect('task.group', 'group', 'group.id = task.groupId')
      .where({
        '"group"."projectId"': projectId,
        localId: localTaskId
      })
      .getOne();
  }

  async findProjectOfTask(localTaskId: string): Promise<Project> {
    const task = await this.taskRepository.findOneOrFail({
      where: {
        localId: localTaskId
      },
      relations: ['group', 'group.project'],
      cache: true
    });
    return task.group.project;
  }

  async save(...tasks: Task[]): Promise<Task[]> {
    return await this.taskRepository.save(tasks);
  }

  async delete(
    criteria: string | string[] | number | number[] | Date | Date[] | ObjectID | ObjectID[] | FindConditions<Task>
  ): Promise<DeleteResult> {
    return await this.taskRepository.delete(criteria);
  }
}
