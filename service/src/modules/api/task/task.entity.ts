import { Entity, Column, ManyToOne, OneToMany, Index, PrimaryGeneratedColumn } from 'typeorm';
import { Group } from '../group/group.entity';
import { Event } from '../event/event.entity';

@Entity()
@Index(['localId', 'groupId'], { unique: true })
export class Task {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ length: 128 })
  name: string;

  @Column({ nullable: false })
  localId: string;

  @Column({ nullable: false })
  groupId: string;

  @ManyToOne(type => Group, group => group.tasks)
  group: Group;

  @Column('jsonb')
  meta: Record<string, any>;

  @Column('jsonb')
  provides: string[];

  @OneToMany(type => Event, event => event.task, { onDelete: 'CASCADE' })
  events: Event[];
}
