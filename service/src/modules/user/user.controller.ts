import { Get, Controller, UseGuards, Req } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiUseTags } from '@nestjs/swagger';
import { UserService } from './user.service';
import { AuthGuard } from '@nestjs/passport';

@Controller('user')
@ApiUseTags('User')
@ApiResponse({ status: 401, description: 'Not authenticated' })
@ApiBearerAuth()
@UseGuards(AuthGuard('bearer'))
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get('/')
  @ApiOperation({ title: 'Get current user info', description: 'Returns user object with projects belonging to it' })
  @ApiResponse({ status: 200, description: 'OK' })
  async getUser(@Req() req): Promise<any> {
    return await this.userService.findWithProjects({ id: req.user.id });
  }
}
