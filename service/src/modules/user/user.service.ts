import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, FindConditions, FindManyOptions } from 'typeorm';
import { User } from './user.entity';
import * as bcrypt from 'bcrypt';
import { logger } from "../../logger";

@Injectable()
export class UserService {
  constructor(@InjectRepository(User) private readonly userRepository: Repository<User>) {}

  async create(user: Partial<User>): Promise<User> {
    user.password = await bcrypt.hash(user.password, 10);
    return this.userRepository.create(user);
  }

  async findWithProjects(conditions: FindConditions<User>) {
    return await this.userRepository.findOne(conditions, { relations: ['projects'] });
  }

  async validate(login: string, password: string): Promise<User> {
    const user = await this.userRepository.findOne({ login }, { select: ['id', 'login', 'password'] });

    if (user && (await bcrypt.compare(password, user.password))) {
      logger.log(`Validated user "${login}" by password`, 'UserService');
      return user;
    }
    logger.warn(`Failed to validate user "${login}" by password`, 'UserService');
  }

  async find(login: string): Promise<User> {
    return await this.userRepository.findOne({ login });
  }

  async findAll(options?: FindManyOptions<User>): Promise<User[]> {
    return await this.userRepository.find(options);
  }

  async save(users: User | User[]): Promise<User | User[]> {
    if (Array.isArray(users)) {
      return await this.userRepository.save(users);
    }
    return await this.userRepository.save(users);
  }
}
