import { Entity, Column, PrimaryGeneratedColumn, OneToMany, ManyToMany, JoinTable } from 'typeorm';
import { Token } from '../auth/token.entity';
import { Project } from '../api/project/project.entity';

@Entity()
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: number;

  @Column({ length: 128, unique: true })
  login: string;

  @Column({ type: 'varchar', select: false })
  password: string;

  @OneToMany(type => Token, token => token.user)
  tokens: string[];

  @ManyToMany(type => Project, project => project.users)
  @JoinTable()
  projects: Project[];
}
