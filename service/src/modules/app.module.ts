import * as path from 'path';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ApiModule } from './api/api.module';
import { UserModule } from './user/user.module';
import { AuthModule } from './auth/auth.module';
import { config } from '../config';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: config.db.host,
      port: config.db.port,
      username: config.db.username,
      password: config.db.password,
      database: config.db.database,
      synchronize: false,
      entities: [path.join(__dirname, '**/*.entity.{ts,js}')],
      migrations: [path.join(__dirname, '../migration/*.{ts,js}')],
      migrationsRun: process.env.NODE_ENV === 'production',
      logging: process.env.NODE_ENV === 'production' ? ['migration', 'schema', 'error', 'warn'] : 'all'
    }),
    AuthModule,
    UserModule,
    ApiModule
  ],
  providers: []
})
export class AppModule {}
