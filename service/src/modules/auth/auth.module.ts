import { Logger, Module } from '@nestjs/common';
import { UserModule } from '../user/user.module';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { HttpStrategy } from './http.strategy';
import { IPGuard } from './ip.guard';
import { Token } from './token.entity';

@Module({
  imports: [UserModule, TypeOrmModule.forFeature([Token])],
  controllers: [AuthController],
  providers: [AuthService, HttpStrategy, Logger, IPGuard],
  exports: [AuthService, IPGuard]
})
export class AuthModule {}
