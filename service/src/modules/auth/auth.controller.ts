import { Body, Controller, ForbiddenException, Post, Headers, Get, UseGuards, Req, Delete, Param } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiExcludeEndpoint, ApiModelProperty, ApiOperation, ApiResponse, ApiUseTags } from '@nestjs/swagger';
import { User } from '../user/user.entity';
import { UserService } from '../user/user.service';
import { AuthService } from './auth.service';
import { Token } from './token.entity';
import { logger } from "../../logger";

class LoginDTO {
  @ApiModelProperty()
  readonly username: string;

  @ApiModelProperty()
  readonly password: string;
}

class RegistrationDTO {
  @ApiModelProperty()
  readonly username: string;

  @ApiModelProperty()
  readonly password: string;
}

@ApiUseTags('Authentication')
@Controller('auth')
export class AuthController {
  constructor(private readonly userService: UserService, private readonly authService: AuthService) {}

  @Post('login')
  @ApiOperation({ title: 'Log in for service administration' })
  @ApiResponse({ status: 201, description: 'Successfully logged in.' })
  @ApiResponse({ status: 403, description: 'Given credentials are not valid.' })
  async performLogin(@Body() loginDTO: LoginDTO, @Headers('user-agent') userAgent: string): Promise<Token> {
    const user: User = await this.userService.validate(loginDTO.username, loginDTO.password);
    if (!user) {
      throw new ForbiddenException('Given user does not exist');
    }
    return this.authService.createNewTokenForUser(user, userAgent);
  }

  @Get('logout')
  @UseGuards(AuthGuard('bearer'))
  async performLogout(@Headers('authorization') authorizationToken, @Req() req) {
    const token = authorizationToken.replace(/bearer\s/i, '');
    await this.authService.revokeToken(req.user, token);
  }

  @Post('register')
  @ApiOperation({ title: 'Register new account' })
  @ApiResponse({ status: 201, description: 'Successfully registered.' })
  async register(@Body() registrationDTO: RegistrationDTO): Promise<User> {
    const user: User = await this.userService.create({
      login: registrationDTO.username,
      password: registrationDTO.password
    });

    const res = (await this.userService.save(user)) as User;
    logger.log(`New user registered: ${res.login}`);
    return res;
  }

  @Delete('revoke/:token')
  @UseGuards(AuthGuard('bearer'))
  async revokeToken(@Headers('authorization') authorizationToken, @Req() req, @Param('token') token) {
    return await this.authService.revokeToken(req.user, token);
  }

  @Get('tokens')
  @UseGuards(AuthGuard('bearer'))
  async tokens(@Headers('authorization') authorizationToken, @Req() req) {
    return await this.authService.getTokens(req.user);
  }

  @Get('test')
  @ApiExcludeEndpoint()
  @UseGuards(AuthGuard('bearer'))
  test() {
    return 'Authorized';
  }
}
