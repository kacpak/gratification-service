import { CanActivate, ExecutionContext, LoggerService } from "@nestjs/common";
import ip from 'ip';

export class IPGuard implements CanActivate {
  constructor(private whitelist: string[] = [], private logger: LoggerService) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const log = msg => this.logger.log(msg, 'IPGuard');

    if (this.whitelist.length === 0) {
      log('Request allowed: no whitelist defined');
      return true;
    }

    const request = context.switchToHttp().getRequest();
    const remoteAddress = request.headers['x-forwarded-for'] || request.connection.remoteAddress;
    const isAllowed = this.whitelist.some(allowedIp => ip.isEqual(remoteAddress, allowedIp));

    if (isAllowed) {
      log(`Request from "${remoteAddress}" allowed. Address matches addresses defined in whitelist.`);
    } else {
      this.logger.warn(`Request from "${remoteAddress}" got denied, due to not being among whitelisted addresses`, 'IPGuard');
    }

    return isAllowed;
  }
}
