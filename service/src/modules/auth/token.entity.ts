import { Entity, Column, Unique, CreateDateColumn, PrimaryColumn, ManyToOne } from 'typeorm';
import { User } from '../user/user.entity';

@Entity()
@Unique(['token'])
export class Token {
  @PrimaryColumn()
  token: string;

  @ManyToOne(type => User, user => user.tokens)
  user: User;

  @Column()
  userAgent: string;

  @CreateDateColumn()
  createdDate: Date;
}
