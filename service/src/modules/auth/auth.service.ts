import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository } from 'typeorm';
import { User } from '../user/user.entity';
import { UserService } from '../user/user.service';
import uuid from 'uuid/v4';
import { Token } from './token.entity';
import { logger } from "../../logger";

@Injectable()
export class AuthService {
  constructor(private readonly userService: UserService, @InjectRepository(Token) private readonly tokenRepository: Repository<Token>) {}

  async validateUser(token: string): Promise<User> {
    const dbToken: Token = await this.tokenRepository.findOne({ token }, { relations: ['user'] });

    if (dbToken) {
      return dbToken.user;
    }
  }

  createNewTokenForUser(user: User, userAgent): Promise<Token> {
    logger.log(`Created new token for user "${user.login}"`, 'AuthService');
    const token = this.tokenRepository.create({
      token: uuid(),
      user,
      userAgent
    });

    return this.tokenRepository.save(token);
  }

  getTokens(user: User): Promise<Token[]> {
    logger.log(`Get tokens of user "${user.login}"`, 'AuthService');
    return this.tokenRepository.find({ user });
  }

  revokeToken(user: User, token: string): Promise<DeleteResult> {
    logger.log(`Revoked token ${token} for user "${user.login}"`, 'AuthService');
    return this.tokenRepository.delete({
      token,
      user
    });
  }
}
