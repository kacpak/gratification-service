import { Strategy } from 'passport-http-bearer';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { AuthService } from './auth.service';
import { logger } from "../../logger";

@Injectable()
export class HttpStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly authService: AuthService) {
    super();
  }

  async validate(token: string, done: (err, payload) => void) {
    // logger.log(`Validating token "${token}"`, 'HttpStrategy');
    const user = await this.authService.validateUser(token);
    if (!user) {
      logger.error(`Unauthorized access`, 'HttpStrategy');
      return done(new UnauthorizedException(), false);
    }
    logger.log(`Validated user "${user.login}" with token`, 'HttpStrategy');
    done(null, user);
  }
}
