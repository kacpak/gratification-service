# Configuring project
* [Creating user account](#creating-user-account)
* [Logging in to get authorization token](#logging-in-to-get-authorization-token)
* [Creating new project](#creating-new-project)
  * [Listing all your projects](#listing-all-your-projects)
  * [Creating task group](#creating-task-group)
  * [Creating tasks](#creating-tasks)
  * [Creating achievements](#creating-achievements)
    * [All perfect](#all-perfect)
    * [Best in 3 tries](#best-in-3-tries)
    * [First perfect score in group](#first-perfect-score-in-group)
* [Sending events](#sending-events)


## Legend
`HOSTNAME` full base url to your application, for example `http://localhost:4000`

`USER` name of your service user  
`USER_ID` id of a service user  
`PASS` password for your user  
`AUTH` authorization token acquired from logging in  

`PROJECT_ID` id of user's project  
`GROUP_ID` id of defined tasks group

# Creating user account
> Service user account is required to use the gratification service 

Request
```bash
curl --request POST \
  --url 'HOSTNAME/api/auth/register' \
  --header 'Content-Type: application/x-www-form-urlencoded' \
  --data 'username=USER&password=PASS'
```

Response **201 Created**
```json
{
    "login": "USER",
    "id": "USER_ID"
}
```

# Logging in to get authorization token
> You need to log in to acquire your authorization token to authenticate your other API calls

Request
```bash
curl --request POST \
  --url 'HOSTNAME/api/auth/login' \
  --header 'Content-Type: application/x-www-form-urlencoded' \
  --data 'username=USER&password=PASS'
```

Response **201 Created**
```json
{
    "token": "AUTH",
    "userAgent": "PostmanRuntime/7.3.0",
    "user": {
        "id": "USER_ID",
        "login": "USER"
    },
    "createdDate": "2018-11-04T07:28:40.983Z"
}
```

# Creating new project
> Project is the basis of service's functionality. It contains achievement and tasks definitions and is referred to in events from your services 

`callbackUrl` when achievement will be granted this URL will be used to sent POST request with this information  
`isABTested` if `true` each new project's user registered by event will have either be registered to have achievements turned on or not, if `false` all users will use achievements

Request
```bash
curl --request POST \
  --url 'HOSTNAME/api/project/' \
  --header 'Authorization: Bearer AUTH' \
  --header 'Content-Type: application/json' \
  --data '{
	"name": "Wstęp do programowania",
	"callbackUrl": "https://webhook.site/f9d4dd67-4651-4c8c-ac45-1e5f0c00a533",
	"isABTested": true
}'
```

Response **201 Created**
```json
{
    "name": "Wstęp do programowania",
    "callbackUrl": "https://webhook.site/f9d4dd67-4651-4c8c-ac45-1e5f0c00a533",
    "isABTested": true,
    "users": [
        {
            "id": "USER_ID",
            "login": "USER"
        }
    ],
    "id": "PROJECT_ID"
}
```

### Listing all your projects
Request
```bash
curl --request GET \
  --url 'HOSTNAME/api/project/' \
  --header 'Authorization: Bearer AUTH' \
  --header 'Content-Type: application/json'
```

Response **200 OK**
```json
[
    {
        "id": "PROJECT_ID",
        "name": "Wstęp do programowania",
        "callbackUrl": "https://webhook.site/f9d4dd67-4651-4c8c-ac45-1e5f0c00a533",
        "isABTested": true
    }
]
```

## Creating task group
> Task group contains all your similar tasks. For example you can create group for *Exercises* and *Exams*. Both this groups can have configured different sets of achievements. So you wouldn't get achievement for turning exam in within one hour from publishing - which is expected here, but is commendable for *Exercises*.  
> All tasks need to be contained in a task group. 

Request
```bash
curl --request POST \
  --url 'HOSTNAME/api/project/PROJECT_ID/group' \
  --header 'Authorization: Bearer AUTH' \
  --header 'Content-Type: application/json' \
  --data '{
	"name": "Ćwiczenia",
	"description": "Ćwiczenia ze wstępu do programowania"
}'
```

Response **201 Created**
```json
[
    {
        "name": "Ćwiczenia",
        "description": "Ćwiczenia ze wstępu do programowania",
        "project": {
            "id": "PROJECT_ID",
            "name": "Wstęp do programowania",
            "callbackUrl": "https://webhook.site/f9d4dd67-4651-4c8c-ac45-1e5f0c00a533",
            "isABTested": true
        },
        "id": "GROUP_ID"
    }
]
```

## Creating tasks
> Task here maps to something that can be evaluated in your service. For example a specific homework exercise in moodle.  
> Task define metadata that constrains them and give context to events. For example start / end date or difficulty.

`taskId` in requests body is used to map directly to your database, to allow for easier sending of events (you pass there your own task id instated of the one created here)  

Request
```bash
curl --request POST \
  --url 'HOSTNAME/api/project/PROJECT_ID/group/GROUP_ID/task' \
  --header 'Authorization: Bearer AUTH' \
  --header 'Content-Type: application/json' \
  --data '{
   "name": "Pętle *for*",
   "taskId": "12",
   "meta": {
      "startsAt": "2018-11-04T09:00:00.163Z",
      "endsAt": "2018-11-11T10:00:00.163Z",
      "maxScore": 100,
      "difficulty": 1
   },
   "provides": [
      "score",
      "tries"
   ]
}'
```

Response **201 Created**
```json
[
    {
        "name": "Pętle *for*",
        "localId": "12",
        "groupId": "GROUP_ID",
        "meta": {
            "startsAt": "2018-11-04T09:00:00.163Z",
            "endsAt": "2018-11-11T10:00:00.163Z",
            "maxScore": 100,
            "difficulty": "easy"
        },
        "provides": [
            "score",
            "tries"
        ],
        "id": "TASK_ID"
    }
]
```

## Creating achievements
> Achievements define conditions that need to be met to get them. They are evaluated when event is processed.

`scope` GROUP = 0 | TASK = 1  
`triggeredBy` a clue for when achievement should be evaluated if event comes with data defined here  
`availableInGroups` achievements are group-bound and can be assigned to multiple of them  
`rules` an array with defined rules for different levels of achievement (array with 1 element will produce achievement with one stage only)

Group scoped achievements need to have rule object with only 1 property - either `all` or `some` to define to how many tasks given rules need to match before giving achievement.

Response **201 Created**  
Data of achievement with its given `id`

### All perfect
> Given when achieved 100% in all tasks in group  

Request
```bash
curl --request POST \
  --url 'HOSTNAME/api/project/PROJECT_ID/achievement' \
  --header 'Authorization: Bearer AUTH' \
  --header 'Content-Type: application/json' \
  --data '{
  "name": "All perfect",
  "description": "Given when achieved 100% in all tasks in group",
  "scope": 0,
  "triggeredBy": [
    "score"
  ],
  "availableInGroups": [
    "GROUP_ID"
  ],
  "rules": [
    {
      "all": {
        "value": "$score",
        "comparator": "==",
        "target": "$maxScore"
      }
    }
  ]
}'
```

### Best in 3 tries
> Given when achieved 100% in max 3 tries

Request
```bash
curl --request POST \
  --url 'HOSTNAME/api/project/PROJECT_ID/achievement' \
  --header 'Authorization: Bearer AUTH' \
  --header 'Content-Type: application/json' \
  --data '{
  "name": "Best in 3 tries",
  "description": "Given when achieved 100% in max 3 tries",
  "scope": 1,
  "triggeredBy": [
    "score",
    "tries"
  ],
  "availableInGroups": [
    "GROUP_ID"
  ],
  "rules": [
    {
      "and": [
        {
          "value": "$score",
          "comparator": "==",
          "target": "$maxScore"
        },
        {
          "value": "$tries",
          "comparator": "<=",
          "target": "3"
        }
      ]
    }
  ]
}'
```

### First perfect score in group
> Given when achieved perfect score in first task in a group

Request
```bash
curl --request POST \
  --url 'HOSTNAME/api/project/PROJECT_ID/achievement' \
  --header 'Authorization: Bearer AUTH' \
  --header 'Content-Type: application/json' \
  --data '{
  "name": "First perfect score in group",
  "description": "Given when achieved perfect score in first task in a group",
  "scope": 0,
  "triggeredBy": [
    "score"
  ],
  "availableInGroups": [
    "GROUP_ID"
  ],
  "rules": [
    {
      "some": {
        "value": "$score",
        "comparator": "==",
        "target": "$maxScore"
      }
    }
  ]
}'
```

# Sending events
> Events are what glues your application with gratification service. When something happens on your site you send this information here.  
> Events are processed asynchronously and if they result in achievement, project's callback url will be POSTed.

`taskId` this is a task id from your database that was earlier mapped to service's tasks  
`actorId` this is id of your site's user that triggered this event (for example a student that submitted homework)  
`occurredAt` this events could take place at different time than when processed, so this is needed to keep track of timinig of events   
`data` this is the actual data that comes with the event, for example the score that was achieved by the student and number of his tries to get here

Request
```bash
curl --request POST \
  --url 'HOSTNAME/api/event/PROJECT_ID' \
  --header 'Authorization: Bearer AUTH' \
  --header 'Content-Type: application/json' \
  --data '{
	"taskId": "189",
	"occurredAt": "2018-11-04T10:55:19.983Z",
	"actorId": "6",
	"data": {
		"score": 90,
		"tries": 1
	}
}'
```

Response **201 Created**  
Empty
