# Installation
* [via Docker](#via-docker)
  * [Get Image](#get-image)
  * [Run](#run)
  * [Test](#test)
* [via Node](#via-node)
* [Next](#next)

# via Docker
## Requirements
* Postgres 10 database
* docker 18 compatible environment

## Get Image

### Option 1: Pull from registry
```bash
$ docker pull registry.gitlab.com/kacpak/gratification-service:latest
$ docker tag registry.gitlab.com/kacpak/gratification-service:latest gratification-service:latest
```

### Option 2: Build
```bash
$ docker build --tag gratification-service:latest .
```

#### [OPTIONAL] To extract build artifacts for inspection
This will extracts built files to `artifacts` directory
```bash
$ docker create --name service-container gratification-service:latest
$ docker cp service-container:/app/. artifacts
```

## Run
replace `{{VAR}}` with proper values 
```bash
$ docker run \
    --name gs \
    --network host \
    -e DB_HOST={{VAR}} \
    -e DB_PORT={{VAR}} \
    -e DB_USER={{VAR}} \
    -e DB_PASS={{VAR}} \
    -e DBATABASE={{VAR}} \
    -e PORT={{VAR}} \
    gratification-service
```

## Test
* Go to `$host:$port` to verify that service is working

# via Node 
TODO

# Next
[Configuring project](./CONFIGURING_PROJECT.md)
