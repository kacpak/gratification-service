import Vue from 'vue';
import Router from 'vue-router';
import store from '@/store';

Vue.use(Router);

export default new Router({
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      redirect: () => ({
        name: store.getters.isLoggedIn ? 'projects' : 'login'
      })
    },
    {
      path: '/login',
      name: 'login',
      component: () => import(/* webpackChunkName: "login" */ './views/Login.vue')
    },
    {
      path: '/register',
      name: 'register',
      component: () => import(/* webpackChunkName: "register" */ './views/Register.vue')
    },
    {
      path: '/user',
      name: 'user',
      component: () => import(/* webpackChunkName: "user" */ './views/Authenticated.vue'),
      children: [
        {
          path: 'tokens',
          name: 'tokens',
          component: () => import(/* webpackChunkName: "user" */ './views/authenticated/Tokens.vue')
        },
        {
          path: 'projects',
          name: 'projects',
          component: () => import(/* webpackChunkName: "projects" */ './views/authenticated/ProjectsList.vue')
        },
        {
          path: 'projects/:id',
          name: 'project-details',
          component: () => import(/* webpackChunkName: "projectDetail" */ './views/authenticated/Project/Project.vue'),
          children: [
            {
              path: 'overview',
              name: 'overview',
              component: () => import(/* webpackChunkName: "overview" */ './views/authenticated/Project/Overview.vue')
            },
            {
              path: 'actors',
              name: 'actors',
              component: () => import(/* webpackChunkName: "actors" */ './views/authenticated/Project/Actors.vue')
            },
            {
              path: 'group',
              name: 'groups',
              component: () => import(/* webpackChunkName: "groups" */ './views/authenticated/Project/GroupsList.vue')
            },
            {
              path: 'group/:groupId',
              name: 'group',
              component: () => import(/* webpackChunkName: "groups" */ './views/authenticated/Project/GroupsList.vue'),
              children: [
                {
                  path: 'task/:taskId',
                  name: 'task',
                  component: () => import(/* webpackChunkName: "group" */ './views/authenticated/Project/Task.vue')
                }
              ]
            },
            {
              path: 'achievements',
              name: 'achievements',
              component: () => import(/* webpackChunkName: "achievements" */ './views/authenticated/Project/Achievements.vue')
            },
            {
              path: 'events',
              name: 'events',
              component: () => import(/* webpackChunkName: "events" */ './views/authenticated/Project/Events.vue')
            }
          ]
        }
      ]
    }
  ]
});
