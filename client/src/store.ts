import { getUserData } from '@/services/user.service';
import Vue from 'vue';
import Vuex, { ActionContext } from 'vuex';
import { User } from '../../service/src/modules/user/user.entity';

Vue.use(Vuex);

interface State {
  token: string | null;
  user: User | null;
}

const initialState: State = JSON.parse(localStorage.getItem('state') || '""') || {
  token: null,
  user: null
};

const store = new Vuex.Store<State>({
  state: initialState,
  getters: {
    isLoggedIn(state): boolean {
      return !!state.token;
    }
  },
  mutations: {
    changeToken(state, token: string | null) {
      state.token = token;
    },
    setUser(state, user) {
      state.user = user;
    }
  },
  actions: {
    async fetchUserData(context: ActionContext<State, State>) {
      const data = await getUserData();
      context.commit('setUser', data);
    }
  }
});

store.subscribe((mutation, state) => {
  localStorage.setItem('state', JSON.stringify(state));
});

export default store;
