import router from '@/router';
import axios, { AxiosResponse } from 'axios';
import { AchievementDto } from '../../../service/src/modules/api/achievements/achievement.dto';
import { Project } from '../../../service/src/modules/api/project/project.entity';
import store from '../store';
import config from '../config';

const http = axios.create({
  baseURL: config.apiHost
});

http.interceptors.response.use(
  res => res,
  error => {
    store.commit('setUser', null);
    store.commit('changeToken', null);
    router.push({ name: 'login' });
    return Promise.reject(error);
  }
);

export async function getUserData() {
  const res: AxiosResponse = await http.get('user', {
    headers: {
      Authorization: `Bearer ${store.state.token}`
    }
  });

  return res.data;
}

export async function getProject(id: string) {
  const res: AxiosResponse = await http.get(`project/${id}`, {
    headers: {
      Authorization: `Bearer ${store.state.token}`
    }
  });

  return res.data;
}

export async function getGroups(projectId: string) {
  const res: AxiosResponse = await http.get(`project/${projectId}/group`, {
    headers: {
      Authorization: `Bearer ${store.state.token}`
    }
  });

  return res.data;
}

export async function addGroup(projectId: string, name: string, description: string) {
  const res: AxiosResponse = await http.post(
    `project/${projectId}/group`,
    {
      name,
      description
    },
    {
      headers: {
        Authorization: `Bearer ${store.state.token}`
      }
    }
  );

  return res.data;
}

export async function addTask(projectId: string, groupId: string, task) {
  const res: AxiosResponse = await http.post(`project/${projectId}/group/${groupId}/task`, task, {
    headers: {
      Authorization: `Bearer ${store.state.token}`
    }
  });

  return res.data;
}

export async function getTasks(projectId: string, groupId: string) {
  const res: AxiosResponse = await http.get(`project/${projectId}/group/${groupId}/task`, {
    headers: {
      Authorization: `Bearer ${store.state.token}`
    }
  });

  return res.data;
}

export async function getTask(projectId: string, groupId: string, taskId: string) {
  const res: AxiosResponse = await http.get(`project/${projectId}/group/${groupId}/task/${taskId}`, {
    headers: {
      Authorization: `Bearer ${store.state.token}`
    }
  });

  return res.data;
}

export async function getAchievements(projectId: string) {
  const res: AxiosResponse = await http.get(`project/${projectId}/achievement`, {
    headers: {
      Authorization: `Bearer ${store.state.token}`
    }
  });

  return res.data;
}

export async function addAchievement(projectId: string, dto: AchievementDto) {
  const res: AxiosResponse = await http.post(
    `project/${projectId}/achievement`,
    {
      ...dto,
      projectId
    },
    {
      headers: {
        Authorization: `Bearer ${store.state.token}`
      }
    }
  );

  return res.data;
}

export async function getEvents(projectId: string) {
  const res: AxiosResponse = await http.get(`event/${projectId}`, {
    headers: {
      Authorization: `Bearer ${store.state.token}`
    }
  });

  return res.data;
}

export async function getActors(projectId: string) {
  const res: AxiosResponse = await http.get(`project/${projectId}/actor`, {
    headers: {
      Authorization: `Bearer ${store.state.token}`
    }
  });

  return res.data;
}

export async function createProject(project: Partial<Project>) {
  const res: AxiosResponse = await http.post('project', project, {
    headers: {
      Authorization: `Bearer ${store.state.token}`
    }
  });

  return res.data;
}
