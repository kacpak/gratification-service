import axios, { AxiosResponse } from 'axios';
import store from '../store';
import config from '../config';

const http = axios.create({
  baseURL: config.apiHost
});

export async function login(username, password) {
  const res = await http.post('auth/login', { username, password });
  store.commit('changeToken', res.data.token);
}

export async function register(username, password) {
  await http.post('auth/register', { username, password });
}

export async function logout() {
  store.commit('setUser', null);
  await http.get('auth/logout', {
    headers: {
      Authorization: `Bearer ${store.state.token}`
    }
  });
  store.commit('changeToken', null);
}

export async function revokeToken(token: string) {
  const res: AxiosResponse = await http.delete(`auth/revoke/${token}`, {
    headers: {
      Authorization: `Bearer ${store.state.token}`
    }
  });

  return res.data;
}

export async function getTokens() {
  const res: AxiosResponse = await http.get('auth/tokens', {
    headers: {
      Authorization: `Bearer ${store.state.token}`
    }
  });

  return res.data.map(token => ({
    ...token,
    createdDate: new Date(Date.parse(token.createdDate)).toLocaleString()
  }));
}
